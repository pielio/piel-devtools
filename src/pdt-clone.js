#!/usr/bin/env node

import program from 'commander';
import fs from 'fs';
import co from 'co';
import PielDT from './libs/PielDT';

program
	.arguments('<url>')
	.option( '--replace', 'Replace existing components')
	.option( '-p, --project', 'Clone as project (clones to your project directory, not your development directory)')
	.option( '-i, --install', 'Install node_modules (yarn install) after cloning is complete')
	.option( '-l, --link', 'Link all development components after install')
	.action( ( url, commander ) => {

		co( function*() {

			let options = {
				url: url,
				replace: !!program.replace,
				project: !!program.project,
				install: !!program.install,
				link: !!program.link
			};

			if(!options.install && options.link ) {
				console.log(chalk.red.bold('Aborting.. argue --install if you want the clone to be linked'));
				process.exit();
			}

			const Instance = new PielDT();
			if(url === 'all') {
				Instance.cloneAll(options);
			} else {
				Instance.clone(options);
			}
		})
		.catch( err => {
			console.log('Exception error', err );
		})

	});

	program.on('--help', function(){
		console.log('  Examples:');
		console.log('');
		console.log('    $ pdt clone mymodule (performs a git clone to the development directory)');
		console.log('    $ pdt clone --project myproject (performs a git clone to the project directory)');
		console.log('    $ pdt clone --project -i myproject (performs a git clone to the project directory, then installs it.)');
		console.log('    $ pdt clone --project -i myproject (performs a git clone to the project directory, then installs it , and finally links all developmental components to it)');
		console.log('');
	});

program.on('--help', function(){
	console.log('  Examples:');
	console.log('');
	console.log('    $ pdt clone git@bitbucket.org/myteam/package.git [clone a package to the packages directory]');
	console.log('    $ pdt clone git@bitbucket.org/myteam/package.git -i [clone a package to the packages directory, and run yarn install over it]');
	console.log('    $ pdt clone git@bitbucket.org/myteam/package.git -il [clone a package to the packages directory, run yarn install over it, then yarn link everything found in the packages directory to it.');
	console.log('    $ pdt clone git@bitbucket.org/myteam/project.git -p [clone a project to the project directory]');

	console.log('');
});

program.parse(process.argv);
