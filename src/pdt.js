#!/usr/bin/env node
import program from 'commander';

program
	.version('0.0.1')
	.command('setup', 'configure PDT for your environment')
	.command('config', 'configure')
	.command('info', 'get package information')
	.command('create', 'create a package from a template')
	.command('install', 'install components')
	.command('publish', 'publish components')
	.command('watch', 'listen for changes on a component')
	.command('clone', 'clone components to your component directory')
	.command('update', 'update a components dependecy to a version')
	.command('link', 'link packages')
	.command('linkinfo', 'get information about yarn linked packages')
	.parse(process.argv);
