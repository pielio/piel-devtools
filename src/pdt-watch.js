#!/usr/bin/env node

import program from 'commander';
import fs from 'fs';
import co from 'co';
import PielDT from './libs/PielDT';

/** @TODO tests! **/
program
	.arguments('[action]')
	.action( ( action, commander ) => {

		co( function*() {

			const Instance = new PielDT();

			switch( action ) {

				case 'compile':
					Instance.watch({ action: ['compile'] });

			}
		})
		.catch( err => {
			console.log('oppz somethign went wrong', err );
		})

	});

program.on('--help', function(){
	console.log('  Examples:');
	console.log('');
	console.log('    $ pdt watch compile (compiles packages whenever their /src directory is modified) ');
	console.log('');
});

program.parse(process.argv);
