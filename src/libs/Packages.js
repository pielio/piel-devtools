import co from 'co';
import fs, {existsSync,readFileSync} from 'fs';
import {execSync} from 'child_process';
import path from 'path';
import rp from 'request-promise-native';
import os from 'os';
import Registry from './Registry';
import chalk from 'chalk';
import {Spinner} from 'cli-spinner';
import prompt, { password } from 'co-prompt';

export default class Packages {

	/**
	* Constructor
	*
	* @param {object} config the configuration.
	**/
	constructor( config ) {

		this._config = config;
		this.registry = new Registry(config);
	}

	/**
	* Check for the existence of a package, remote or local.
	*
	**/
	exists( name ) {

		return co( function*() {

			const local = yield this.isLocal(name);
			if(local) return true;

			return yield this.isRemote(name);

		}.bind(this));
	}

	/**
	* Check to see an argued package is local.
	*
	* @param {string} name the package name
	*
 	* @return {boolean} true if the package exists locally, else false.
	**/
	isLocal( name ) {

		return co( function*() {

			const {dev_directory} = this._config
			const package_location = path.join(dev_directory, name);

			return existsSync(package_location);

		}.bind(this));
	}

	/**
	* Check to see if an argued package is remote.
	*
	* @param {string} name the package name
	*
	* @return {boolean} false if the package exists locally, else true.
	**/
	isRemote( name ){

		return co( function*() {

			const{ registry_version } = this._config;
			const registry_url = yield this.registry.configured_registry();

			const url = registry_version === 'v1'
				? registry_url + '/-/search/' + name
				: registry_url + '/-/verdaccio/search/' + name;

			const response = yield rp.get({url:url, json:true});

			if(!response.length) return false;

			return response.some( pkg => {
				return pkg.name === name;
			});

		}.bind(this));
	}

	isLinkable( name ) {

		return co( function*() {

			const isLocal = yield this.isLocal(name);
			if(!isLocal) return false;

			return existsSync( path.join(os.homedir(), '.config', 'yarn', 'link', name) );

		}.bind(this));
	}

	linkLocation( name ) {

		return co( function*() {

			const isLocal = yield this.isLocal(name);
			if(!isLocal) return false;

			const directory = yield this.directory(name);
			const initial_dir = process.cwd();

			const linked_dir = path.join(os.homedir(), '.config', 'yarn', 'link');
			const linked_components = fs.readdirSync(linked_dir);

			let exists = !!linked_components.find( comp => { return comp === name });
			if(!exists) return false;

			return fs.realpathSync(linked_dir + '/' + name, 'utf8');

		}.bind(this));
	}

	directory( name ) {

		return co( function*() {

			const {dev_directory, project_directory} = this._config;
			const isProject = yield this.isProject(name);
			return path.join(isProject ? project_directory : dev_directory, name);

		}.bind(this));
	}

	/**
	* Check to see if an argued package is a project
	*
	* @param {string} name the package name.
	*
	* @return {boolean} true if project, false otherwise.
	**/
	isProject( name ) {

		return co( function*() {

			const {dev_directory, project_directory} = this._config;
			const project_dir = path.join(project_directory, name);
			const dev_dir = path.join(dev_directory, name);

			let pj = existsSync(project_dir);
			let dev = existsSync(dev_dir);

			if(!pj && !dev)
				throw('Package ' + name + ' is not in local development, not a project or a package');

			return pj;

		}.bind(this));
	}

	/**
	* Check to see if an argued package is a package (ie. an available dependency)
	*
	* @param {string} name the package name.
	*
	* @return {boolean} true if package, false otherwise.
	**/
	isPackage( name ) {

		return co( function*() {

			const {dev_directory, project_directory} = this._config;
			const project_dir = path.join(project_directory, name);
			const dev_dir = path.join(dev_directory, name);

			let pj = existsSync(project_dir);
			let dev = existsSync(dev_dir);

			if(!pj && !dev)
				throw('Package ' + name + ' is not in local development, not a project or a package');

			return dev;

		}.bind(this));
	}

	/**
	* get parsed package.json contents for an argued package.
	*
	* @param {string} name the name of the package.
	*
	* @returns {object} the parsed package.json file for this package.
	**/
	localPackageConfig( name ) {

		return co( function*() {

			const {dev_directory} = this._config;
			const package_directory = path.join(dev_directory, name);
			if(!fs.existsSync(package_directory)) throw('Module is expected to exist at ' + package_directory + ' but does not.');

			const package_location = path.join(package_directory, 'package.json');
			if(!fs.existsSync(package_location)) throw('Module has no package.json - is this a legitimate javascritp module? - PDT expects ' + package_location + ' to exist.');

			const contents = readFileSync(package_location, 'utf-8');
			return JSON.parse(contents);

		}.bind(this));
	}

	/**
	* Write package contents.
	* @param {object} args the argument object.
	* @param {string} args.name the name of the packages
	* @param {object} args.content the packgae.json contenst as an object to write.
	*
	**/
	writePackageConfig( args ) {

		return co( function*(){

			const {name, content} = args;

			const initial_dir = process.cwd();
			const target_dir = yield this.directory(name);
			const target_file = target_dir + '/package.json';

			process.chdir(target_dir);
			fs.writeFileSync(target_file, JSON.stringify(content,null,2));
			process.chdir(initial_dir);

		}.bind(this));
	}

	/**
	*
	* get parsed package.json contents for an argued package.
	*
	* @param {string} name the name of the package
	* @param {boolean} view use npm view, rather than the search which is slower, do NOT use this for the exact contents and therfore, default : false.
	*
	* @return {object} the parsed package.json file for this package.
	**/
	remotePackageConfig( name, view ) {

		return co( function*() {

			if(view) {

				const shreturn = execSync('npm view ' + name + ' --json');
				return JSON.parse(shreturn.toString());
			}


			//could use NPM veiw now - which is faster.
			const {registry_version} = this._config;
			const registry_url = yield this.registry.configured_registry();
			const url = registry_version === 'v1'
				? registry_url + '/-/search/' + name
				: registry_url + '/-/verdaccio/search/' + name;

			const response = yield rp.get({url:url, json:true});
			return response.find( itm => { return itm.name === name });

		}.bind(this));
	}

	/**
	* Get the local version (according to package.json).
	*
	* @param {string} name the name of the package.
	*
	* @return {string} the version found in package.json.
	**/
	localVersion( name ) {

		return co( function*() {

			const contents = yield this.localPackageConfig(name);
			return contents.version;
		}.bind(this));

	}

	localDependencyVersion(name, dependencyname ) {

		return co( function*() {

			let pkgcontents = yield this.localPackageConfig(name, true);
			const {dependencies, devDependencies} = pkgcontents;

			if(dependencies && dependencies[dependencyname]) return dependencies[dependencyname];
			if(devDependencies && devDependencies[dependencyname]) return devDependencies[dependencyname];

		}.bind(this));

	}

	/**
	* Get the remote Version of a package (according to verdaccio > npm)
	*
	* @param {string} name the name of the package.
	*
	* @return {string} the version found on veradccio, or npm.
	**/
	remoteVersion( name ) {

		return co( function*() {

			const contents = yield this.remotePackageConfig(name);
			return contents.version;
		}.bind(this));
	}

	/**
	* Get the version of a dependency in use by a package
	*
	* @param {string} name the name of the package.
	* @param {string} dependencyname the name of the dependency to retreive the version in use.
	**/
	remoteDependencyVersion( name, dependencyname ){

		return co( function*() {

			let pkgcontents = yield this.remotePackageConfig(name, true);
			const {dependencies, devDependencies} = pkgcontents;

			if(dependencies && dependencies[dependencyname]) return dependencies[dependencyname];
			if(devDependencies && devDependencies[dependencyname]) return devDependencies[dependencyname];

		}.bind(this));


	}

	/**
	* Check a local package for new versions in git.
	*
	* @param {string} name the name of the package to check.
	*
	* @return {boolean} true if there are newer commits than this local version in the remote origin/master.
	**/
	hasNewerCommits( name ) {

		return co( function*() {

			const {dev_directory} = this._config;

			const initial_dir = process.cwd();
			const target_dir = path.join(dev_directory, name);

			process.chdir(target_dir);
			const resp = execSync('git status');
			process.chdir(initial_dir);
			return resp.toString().indexOf('Your branch is up-to-date with') === -1;

		}.bind(this));

	}

	/**
	* Check for a newer version in the upstream remote/origin.
	*
	* @param {string} name the name of the package.
	* @param {string} version OPTIONAL the version of the package, when not provided the method will retrieve the local version.
	*
	* @return {boolean} true if there is a newer version remotely, false otherwise.
	**/
	hasNewerVersion( name, version ) {

		return co( function*() {

			const isLocal = yield this.isLocal(name);
			if(!version && !isLocal) {
				console.log( chalk.red.bold( 'Package ' + name + ' is not local and therefore a local/remote version comparison is impossible'));
				return;
			}

			 if(!version) {
				 const contents = yield this.localPackageConfig(name);
				 version = contents.version;
			 }

			 const remote_contents = yield this.remotePackageConfig(name);
			 const remote_version = version;

			 return version !== remote_version;

		}.bind(this))
	}

	/**
	* Check to see if a package has commits ready to push.
	*
	* @param {string} name the name of the package.
	*
	* @return {boolean} true if there are commits unpushed, false otherwise.
	**/
	hasCommits(name) {

		return co( function*() {

			const {dev_directory} = this._config;

			const initial_dir = process.cwd();
			const target_dir = path.join(dev_directory, name);

			process.chdir(target_dir);
			const resp = execSync('git log @{u}..HEAD');
			process.chdir(initial_dir);
			return !!resp.toString().length;

		}.bind(this));

	}

	/**
	* Get un pushed commits for a package.
	*
	* @param {object} args the argument object.
	* @param {string} args.name the name of the package.
	* @param {boolean} args.project this is a project package, default auto-determine.
	* @param {boolean} args.lite this returns an array of [<commithash> - message>] insetead
	*
	**/
	getUnpushedCommits(args) {

		return co( function*() {

			const {name, log, lite} = args;
			let {project} = args;
			const {dev_directory, project_directory} = this._config;

			if(project === undefined) project = yield this.isProject(name);
			const inital_dir = process.cwd();
			const target_directory = path.join(project ? project_directory : dev_directory, name);

			process.chdir(target_directory);
			const ret = execSync('git log @{u}..HEAD').toString();

			let emptyCount = 0;
			let logicalCommit = [];

			let out = ret.split('\n').reduce((c, partial) => {

				if(!partial.length) emptyCount++;
				logicalCommit.push(partial);

				if(emptyCount === 2){
					c.push(logicalCommit);
					emptyCount = 0;
					logicalCommit = [];
				}
				return c;
 			}, []);

			if(lite) {
				out = out.map( commit => {
					return commit[0] + ' - ' + commit[4];
				});
			}

			return out;

		}.bind(this));
	}

	/**
	* Check to see if a package has uncommitd changes.
	*
	* @param {string} name the name of the package.
	*
	* @param {boolean} true if there are uncommited changes, false otherwise.
	**/
	hasChanges(name) {

		return co( function*() {

			const {dev_directory} = this._config;

			const initial_dir = process.cwd();
			const target_dir = path.join(dev_directory, name);

			process.chdir(target_dir);
			const resp = execSync('git diff');
			process.chdir(initial_dir);
			return !!resp.toString().length;

		}.bind(this));
	}

	/**
	* Get changed, uncommited files.
	*
	* @param {string} name the package name.
	*
	* @return {array} an array of filenames that have changes ready to commit.
	**/
	changedFiles(name) {

		return co( function*() {

			const {project_directory, dev_directory} = this._config;
			const initial_dir = process.cwd();
			const isProject = yield this.isProject(name);
			const target_dir = path.join(isProject ? project_directory : dev_directory, name);

			process.chdir(target_dir);

			let command = 'git diff';
			const resp = execSync(command).toString();
			let lines = resp.split('\n');
			let files = lines.reduce((c,line) => {
				if(c.length) return c;
				if(line.substr(0,3) === '---') {
					let splt = line.split('/');
					splt.shift()
					c.push(splt.join('/'));
				}
				return c;
			}, []);

			return files;
		}.bind(this))
	}

	hasChangedPackageJSON(name, project) {

		return co( function*() {

			const initial_dir = process.cwd();
			const target_dir = project ? project_directory : dev_directory;

			process.chdir(target_dir);
			const resp = execSync('git status').toString();

			return resp.indexOf('package.json') !== -1;

		}.bind(this));
	}

	getRemotePackageJSON() {

		return co( function*(){

		}.bind(this));
	}

	/**
	* Get the packages in the component directory
	*
	* @param {object} args the argument object
	* @param {boolean} args.config return an array of the package config (ie. an array of parsed package.json), default false.
	* @return {array} a list of package names
	**/
	getLocalPackages( args ) {

		return co( function*() {

			args = args || {};
			const {config} = args;

			const {dev_directory} = this._config;
			const packages = fs.readdirSync(dev_directory);
			if(!config) return packages;
			return yield packages.map( pkg => {
				return this.localPackageConfig(pkg);
			})

		}.bind(this));
	}

	/**
	* Get the packages on the remote server (ie npm/sinopia/verdaccio )
	*
	* @param {object} args the argument object
	* @param {boolean} args.config return an array of the package config (ie. an array of parsed package.json), default false.
	* @return {array} a list of package names
	**/
	getRemotePackages( args ) {

		return co( function*() {

			args = args || {};
			const {config, silent} = args;
			const {yellow} = chalk;
			const {registry, registry_version} = this._config;
			let registry_url,
				names,
				url;

			registry_url = yield this.registry.configured_registry();

			url = registry_version === 'v1'
				? registry_url + '/-/search/*'
				: registry_url + '/-/verdaccio/packages';

			console.log('URISN', url);

			let spinner;
			if(!silent){
				spinner = new Spinner(yellow.bold('Retrieving remote package information.. %s'));
				spinner.setSpinnerString('-/|\\');
				spinner.start();
			}

			const response = yield rp.get({ url: url, json: true });
			names = response.map( pkg => { return pkg.name });

			if(!silent) spinner.stop(true);

			if(config)	return response;
			else 		return names;

		}.bind(this));

	}

	/**
	* Console out a tree of the downstream dependents.
	*
	* @param {string} name the name of the package
	* @param {array} tree OPTIONAL the downstream dependent tree as provided from this.getDependentPackages(), if not provided this method will retrieve them through this method..
	*
	* @return void.
	**/
	logDownstreamDependents(name, tree) {

		return co( function*() {

			const {yellow} = chalk;

			const func = function( pkgs, itr ) {

				itr = itr || 0;
				let cnt = itr;
				let tabs = '';
				while(cnt){ tabs += '  '; cnt--; }

				pkgs.forEach( pkg => {

					console.log(yellow( tabs + '-' + pkg.name));
					if(pkg.deps && pkg.deps.length) {
						func(pkg.deps, itr+1);
					}
				})
			}

			let dependents;
			if(tree) 	dependents = tree;
			else 		dependents = yield this.getDependentPackages({name: name, tree: true});

			console.log(yellow.bold(name + ' has downstream packages:'));
			func(dependents,1);

		}.bind(this));
	}

	/**
	* Get downstream dependents of this package.
	*
	* @param {object} args The argument object.
	* @param {string} args.name The package name
	* @param {boolean} args.tree include dependents of depdents (within the private registry - ie NOT npm.org modules), default FALSE.
	* @param {boolean} args.silent perform withough console output, default false.
	* @param {object} args._remotePackageList used when gathering a tree - saves us from having to retrieve from the server per iteration.
	* @param {object} args._parent
	**/
	getDependentPackages( args ) {

		return co( function*() {

			const {name, tree, silent, _remotePackageList} = args;
			const {yellow} = chalk;

			const remote_packages = _remotePackageList || (yield this.getRemotePackages({config: true, view: true}));
			let remote_dependent_packages = remote_packages.filter( pkg => {
				if(pkg.dependencies && pkg.dependencies[name]) return true;
				if(pkg.devDependencies && pkg.devDependencies[name]) return true;
			});

			const local_packages = yield this.getLocalPackages({config: true});
			let local_dependent_packages = local_packages.filter( pkg => {
				//filter out, it was found remotely.
				if(remote_dependent_packages.find(rpkg => { return rpkg.name === pkg.name })) return false;

				if(pkg.dependencies && pkg.dependencies[name]) return true;
				if(pkg.devDependencies && pkg.devDependencies[name]) return true;
			});

			const dependent_packages = remote_dependent_packages.concat(local_dependent_packages);

			const dependent_package_detail = yield dependent_packages.map( pkg => {

				return co( function*() {

					let o = { name: pkg.name };
					o.local = yield this.isLocal(pkg.name);
					o.remote_version = yield this.remoteVersion(pkg.name);

					if(o.local) {
						o.local_version = yield this.localVersion(pkg.name);
						o.hasChanges = yield this.hasChanges(pkg.name);
						o.hasCommits = yield this.hasCommits(pkg.name);
						o.hasNewerCommits = yield this.hasNewerCommits(pkg.name);
						o.hasNewerVersion = yield this.hasNewerVersion(pkg.name);
					}

					if(tree) {
						o.deps = yield this.getDependentPackages({name: pkg.name, tree: true, silent: true, _remotePackageList : remote_packages });
					}
					return o;
				}.bind(this))
			});

			return dependent_package_detail;

		}.bind(this));
	}

	/**
	* collapse a dependency tree (gained wthrough this.getDependentPackages) into a single array, ordered in a safe commitable order.
	*
	* @param {object} args the argument object.
	* @param {array} args.deps the dependency tree.
	* @param {array} args._deplist
	* @param {object} args._parent
	**/
	collapseDepdencyTree( args ) {

		return co( function*() {

			const {_parent} = args;
			let {deps, _deplist} = args;

			_deplist = _deplist || [];
			deps = JSON.parse(JSON.stringify(deps));

			for( var i=0; i<deps.length; i++ ) {

				let dep = deps[i];
				if(dep.deps && dep.deps.length){

					_deplist = yield this.collapseDepdencyTree({ deps: dep.deps, _deplist: _deplist });
					dep.deps = dep.deps.map( dep => { return dep.name; });
					_deplist = _deplist.concat([dep]);

				} else {

					if(!_deplist.find( d => { return dep.name === d.name })) {

						_deplist = _deplist.concat([dep]);
					}

				}
			}

			return _deplist;

		}.bind(this));
	}


	/**
	* Push the package to the repository on its current branch (git)
	*
	* @param {object} args the argument object
	* @param {string} args.name the name of the package
	* @param {string} args.branch the branchname, default current branch
	* @param {string} args.verbose console the output in its entirity, default false.
	* @param {boolean} args.silent console no out, default true.
	*
	* @todo branch selection
	**/
	pushToRepository( args ) {

		return co( function*() {

			const {yellow,black, bgWhite} = chalk;
			const {name, verbose} = args;
			let {branch, silent} = args;

			const initial_dir = process.cwd();
			const target_dir = yield this.directory(name);

			silent = silent === undefined ? true : silent;

			let command,
				spinner;

			process.chdir(target_dir);
			command = 'git push';

			if(silent && !verbose){
				spinner = new Spinner('Git pushing ' + name + '.. %s');
				spinner.setSpinnerString('|/-\\');
				spinner.start();
			}

			silent ? execSync(command,{stdio: 'ignore'}) : execSync(command,{stdio: 'inherit'});
			process.chdir(initial_dir);

			if(spinner) spinner.stop(false);


		}.bind(this));
	}

	/**
	* Publish the package to the registry (verdaccio/npm)
	*
	* @param {object} args the argument object
	* @param {string} args.name the package name
	* @param {boolean} args.silent console the output, default true.
	*
	**/
	publishToRegistry( args ) {

		return co( function*() {

			const {black, bgWhite} = chalk;
			const {name} = args;
			let {silent} = args;

			const initial_dir = process.cwd();
			const target_dir = yield this.directory(name);

			let command;

			process.chdir(target_dir);
			command = 'npm publish';
			silent ? execSync(command) : execSync(command, {stdio: 'inherit'});
			process.chdir(initial_dir);

		}.bind(this));
	}

	/**
	* clone package locally.
	*
	* @param {object} args the argument object.
	* @param {string} args.name the name of the package to clone.
	* @param {boolean} args.project the package is a project, default false.
	* @param {boolean} args.silent perform silently without console ouptut, default false.
	**/
	clone( args ) {

		return co( function*() {

			const {name,project,silent} = args;
			const {dev_directory, project_directory,team,repository,repository_url} = this._config;

			const initial_dir = process.cwd();
			const target_dir = project ? project_directory : dev_directory;

			if(!repository) {

				console.log(chalk.red.bold('pdt configuration does not have a repository set --- run pdt config set repository `bitbucket|github|custom`' ));
				if(!repository_url)
					console.log(chalk.red.bold('pdt configuration does not have a repository url set --- run pdt config set repository_url `bitbucket.org|github.com|yourcustomdomain.com`' ));

				process.exit();
			}

			if(!repository_url) {

				console.log(chalk.red.bold('pdt configuration does not have a repository url set --- run pdt config set repository_url `bitbucket.org|github.com|yourcustomdomain.com`' ));
				process.exit();
			}

 			if(!team) {

				console.log(chalk.red.bold('pdt configuration does not have a team set --- run pdt config set team `teamname`'));
				process.exit();
			}

			const url = 'git@' + repository_url + ':' + team + '/' + name + '.git';
			const treated_url = this._treatedURL(url);

			process.chdir(target_dir);

			let cmd = 'git clone ' + treated_url;
			silent
				? execSync(cmd, {stdio: 'ignore'})
				: execSync(cmd, {stdio: 'inherit'});

			process.chdir(initial_dir);

		}.bind(this));
	}

	/**
	* Commit package
	*
	* @param {object} args the argument object.
	* @param {string} args.name the package name
	* @param {string} args.message OPTIONAL, a message to commit with , default "auto-generated commit message - no comment"
	* @param {array} args.files OPTIONAL, an array of files to commit, default 'ALL' - eg git commit . -A
	* @param {boolean} args.silent console out command output, default true.
	**/
	commit( args ) {

		return co( function*() {

			const {name, files} = args;
			const {yellow, cyan, red, black, bgWhite} = chalk;

			const initial_dir = process.cwd();

			let {message, silent} = args;

			message = message || 'Auto-generated message by PDT, no comment.';

			silent = silent === undefined ? true : silent;

			const target_dir = yield this.directory(name);
			let command;

			process.chdir(target_dir);

			//roll up the commit.
			command = files
				? files.reduce((c,fn) => { c += ' ' + fn }, 'git add')
				: 'git add . -A';
			silent ? execSync(command) : execSync(command, {stdio: 'inherit'});

			//commit
			command = 'git commit -m "' + message + '"';
			silent ? execSync(command) : execSync(command, {stdio: 'inherit'});

		}.bind(this));
	}

	/**
	* bump a version, pushing all commits, and perform optional server operatios (git, npm etc)
	*
	* @param {object} args the argument object.
	* @param {string} args.name the name of the package.
	* @param {string} args.version the verison type to bump one of - 'MAJOR'|'MINOR'|'PATCH', default 'PATCH'.
	* @param {object} args.manifest a list of packages which have their target_version attached so as to update the dependencies as well.
	* @param {boolean} args.project the packge is a project.
	* @param {boolean} args.silent perform this with no console output, default false.
	**/
	bumpVersion( args ) {

		return co( function*() {

			const {name, silent, manifest} = args;
			const {yellow, cyan, red, black, bgWhite} = chalk;
			let {version} = args;

			version = version || 'PATCH';

			if(version !== 'MAJOR' && version !== 'MINOR' && version !== 'PATCH' ) {
				console.log(red.bold('Invalid version, expected MAJOR|MINOR|PATCH but got ' + version));
				process.exit();
			}

			let isLocal = yield this.isLocal(name);
			if(!isLocal)
				yield this.clone(args);

			const original_contents = yield this.localPackageConfig(name);
			let contents = JSON.parse(JSON.stringify(original_contents))

			contents.version = this.build_new_version(contents.version, version);

			if(manifest) {

				if(contents.dependencies && Object.keys(contents.dependencies).length) {

					contents.dependencies = Object.keys(contents.dependencies).reduce( (c,key) => {
						 let has = manifest.find( mdep => { return mdep.name === key; })
						 if(has && !silent) console.log( yellow('    - dependency ' + has.name + ' updated from ' + contents.dependencies[key] + ' to ' + has.target_version ) );
						 c[key] = has ? '^'+has.target_version : contents.dependencies[key];
						 return c;
					},{});
				}

				if(contents.devDependencies && Object.keys(contents.devDependencies).length) {

					contents.devDependencies = Object.keys(contents.devDependencies).reduce( (c,key) => {
						 let has = manifest.find( mdep => { return mdep.name === key; })
						 if(has && !silent) console.log( yellow('    - devDependency ' + has.name + ' updated from ' + contents.devDependencies[key] + ' to ' + has.target_version ) );
						 c[key] = has ? '^'+has.target_version : contents.devDependencies[key];
						 return c;
					},{});
				}

			}

			yield this.writePackageConfig({ name: name, content: contents});

		}.bind(this));
	}

	/**
	* Get a treated url (replaces the location with an ssh_id ) eg git@bitbucket.org/team/project.git becomes git@myid/team/project.git.
	*
	* @param {string} url the url for the git repository, eg 'git@bitbucket.org/myteam/mypackage.git'
	*
	* @return {string} the ssh_id replaced version of the argued url, eg 'git@mysshid/myteam/mypackage.git'
	**/
	_treatedURL( url ) {

		const {use_ssh_id, ssh_id} = this._config;

		let treated_url = url;
		if(use_ssh_id) {
			treated_url = url.indexOf('@') === -1
				? url
				: url.replace( url.split('@')[1].split('/')[0].split(':')[0], ssh_id);
		}

		return treated_url;
	}

	/**
	* Get a bumped version based off an argued version.
	*
	* @param {string} version the current version
	* @param {string} bump either 'MAJOR'|'MINOR'|'PATCH', default 'PATCH'
	*
	* @return {string} the bumped version.
	**/
	build_new_version( version, bump ) {

		let versionSplit = version.split('.');
		switch(bump.toUpperCase()) {

			case 'PATCH':
				versionSplit[2]++;
				return versionSplit.join('.');

			case 'MINOR':
				versionSplit[1]++;
				return versionSplit.join('.');

			case 'MAJOR':
				versionSplit[0]++;
				return versionSplit.join('.');

			default:
				console.log(chalk.red.bold('Packages::build_new_version --invalid bump type ' + bump + ' use MAJOR|MINOR|PATCH.'));
				process.exit();


		}


	}
}
