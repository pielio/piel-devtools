import co from 'co';
import prompt, { password } from 'co-prompt';

export default class GithubAPI {

	 constructor() {

	 }

	 initialize( config ) {
		 this._config = config;
	 }

	 setup( ) {

		 return co( function*() {

			 console.log('Github not implemented yet. Dont worry! You will be prompted to complete steps until it is.');
			 return this._config;

		 }.bind(this));


	 }

	 /**
	 * Create a repository on Github
	 *
	 * @param {object} args the argument object.
	 * @param {string} args.name the name for the new repository
	 *
	 **/
	 create_repo( args ) {

		 return co( function*() {

			 console.log('Automated creation of Github repositories is not enabled yet. Manually create the repository `' + name + '` through the interface before continuing.' )
			 yield prompt('Done? [YES/no]:');


		 }.bind(this));

	 }
 }
