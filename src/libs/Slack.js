import co from 'co';
import rp from 'request-promise-native';

export default class Slack {

	constructor(config) {

		this._config = config;
		console.log('CONSTRTUCTED SLACK', JSON.stringify(this._config));
	}

	/**
	 * 
	 * @param {Object} args the argument object.
	 * @param {String} args.text the message. 
	 */
	message( args ) {

		//https://api.slack.com/incoming-webhooks

		return co( function*() {

			const {text, emoji} = args;
			const {slack_hook} = this._config;

			let body = {};
			if(text) body.text = text;

			const response = yield rp({
				method: 'POST',
				url: slack_hook,
				body: JSON.stringify(body)
			});

			return response;

		}.bind(this));

	}

}
