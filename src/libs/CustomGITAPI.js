import co from 'co';
import prompt, { password } from 'co-prompt';

export default class CustomGITAPI {

	 constructor() {

	 }

	 initialize( config ) {
		 this._config = config;
	 }

	 setup( ) {

		 return co( function*() {

			 console.log('Custom Git API not implemented yet. Dont worry! You will be prompted to complete steps until it is.')
			 return this._config;
		 }.bind(this));


	 }

	 /**
	 * Create a repository on Custom GIT servers.
	 *
	 * @param {object} args the argument object.
	 * @param {string} args.name the name for the new repository
	 *
	 **/
	 create_repo( args ) {

		 return co( function*() {

			 console.log('Automated creation of repositories is not enabled yet. Manually create the repository `' + name + '` on your git server.' )
			 yield prompt('Done? [YES/no]:');


		 }.bind(this));

	 }
 }
