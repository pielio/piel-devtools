import fs from 'fs';
import co from 'co';
import path from 'path';
import prompt, { password } from 'co-prompt';
import chalk from 'chalk';
import os from 'os';
import watch from 'node-watch';
import rp from 'request-promise-native';
import BitbucketAPI from './BitbucketAPI';
import GithubAPI from './GithubAPI';
import CustomGITAPI from './CustomGITAPI';
import Slack from './Slack';
import Packages from './Packages';

import {exec, execSync} from 'child_process';

export default class PielDT {

	constructor() {

		let config = this._configuration();

		this._hasRCConfig = !!config;
		this._config = config;
		this.instantiateRepoClass();
		this.pkg = new Packages(config);
		this.slack = new Slack(config);
	}

	instantiateRepoClass() {

		return co( function*() {

			if(this._hasRCConfig && this._config.repository_type) {

				switch(this._config.repository_type) {

					case 'bitbucket':
						this.repo_instance = new BitbucketAPI();
						this.repo_instance.initialize(this._config);
						break;

					case 'github':
						this.repo_instance = new GithubAPI();
						this.repo_instance.initialize(this._config);
						break;

					case 'custom':
						this.repo_instance = new CustomGITAPI();
						this.repo_instance.initialize(this._config);
						break;

				}
			}
		}.bind(this))
	}

	/**
	* Run the PDT configuration and install process
	**/
	setup() {

		return co( function*() {

			let config = {};
			let confirm;

			//location of the project Directory
			console.log( chalk.yellow('Lets set up the location of your projects'));
			let project_directory = yield prompt( chalk.cyan('Project directory [' + process.cwd() + ']: '));
			project_directory = project_directory || process.cwd();
			config.project_directory = project_directory;
			console.log();

			//location of dev Directory
			console.log( chalk.yellow('Lets set up the directory where all your node packages will be for linking into the main projects.'));
			let package_directory = yield prompt( chalk.cyan( 'Package directory [' + process.cwd() + ']: ') );
			package_directory = package_directory || process.cwd();
			config.dev_directory = package_directory;
			console.log();

			//SSH IDs
			console.log( chalk.yellow('SSH can be configured to have host based keys, if you dont know what this is, use NONE.'))
			let sshid = yield prompt( chalk.cyan('SSH ID [NONE]'));
			sshid = sshid || 'NONE';
			config.use_ssh_id = sshid === 'NONE' ? false : true;
			config.ssh_id = sshid;

			//verdaccio
			let registry_url, private_npm;

			confirm = yield prompt( chalk.cyan('Private NPM registries (ie verdaccio) can be used, are you using one? [yes/NO]') );
			if( confirm && confirm.toUpperCase() === 'YES' ) {

				confirm = yield prompt( cyan('Registry Type: [VERDACCIO/npmjs/sinopia]'));
				if(!confirm) confirm = 'verdaccio';
				config.registry_type = confirm.toLowerCase();

				if(confirm.toLowerCase() === 'verdaccio') {
					let registry_version = prompt( cyan('Verdaccio Version: [v1/V2]: '));
					if(!registry_version) registry_version = 'v2';
					config.registry_version = registry_version.toLowerCase();
				}
				else {
					config.registry_version = 'na';
				}

				const current_registry = yield this._getNPMServer();
				confirm = yield prompt( chalk.cyan('Use current registry (' + current_registry +') [yes/NO]: ') );

				if( confirm && confirm === 'YES' ) {
					registry_url = current_registry;
					private_npm = true;
				} else {

					registry_url = yield prompt( chalk.cyan('Private Registry URL [CANCEL]: ' ) );
					if(!registry_url || registry_url.toUpperCase() === 'CANCEL') {
						registry_url = 'https://registry.npmjs.org/';
						private_npm = false;
					} else {
						private_npm = true;
					}
				}

			} else {
				private_npm = false;
			}

			config.private_npm = private_npm;
			config.registry_url = registry_url;

			//templates
			let configured_templates,
				templates;

			templates = {};
			confirm = yield prompt( chalk.cyan('Will you be using templates (ie boilerplates)? [YES/no] ') );
			if(confirm && confirm.toUpperCase() === 'NO') {
				configured_templates = false;
			} else {

				confirm = yield prompt( chalk.cyan('Where the are the templates stored: [BITBUCKET/github/custom]:' ) )

				switch(confirm.toString().toUpperCase()) {

					case '':
					case 'BITBUCKET':
						config.repository_type = 'bitbucket';
						const Bitbucket = new BitbucketAPI();
						Bitbucket.initialize(config);
						config = yield Bitbucket.setup();
						break;

					case 'GITHUB':
						config.repository_type = 'github';
						const Github = new GithubAPI();
						Github.initialize(config);
						config = yield Github.setup();
						break;

					case 'CUSTOM':
						config.repository_type = 'custom';
						const CustomGIT = new CustomGITAPI();
						CustomGIT.initialize(config);
						config = yield CustomGIT.setup();
						break;

					default:
						console.log( chalk.red.bold( 'Unknown repository type ' + confirm.toString() ));
						process.exit();
				}


				configured_templates = true;
				let addingTemplates = true;
				confirm = yield prompt( chalk.cyan('Would you like to add some templates now? [yes/NO] ') );
				if(confirm && confirm.toUpperCase() === 'YES') {
					while(addingTemplates) {

						let template_name = yield prompt( chalk.cyan('Template name:') );
						let template_url = yield prompt( chalk.cyan('Git URL:') );

						templates[template_name] = { url : template_url };
						confirm = yield prompt( chalk.cyan('Add another? [yes/NO]: '));
						if(!confirm || confirm.toUpperCase() !== 'YES') {
							addingTemplates = false;
						}
					}
				}
			}
			config.configured_templates = configured_templates;
			config.templates = templates;
			console.log();

			//slack
			confirm = prompt(cyan('Would you like to set up PielDT for Slack [yes/NO]'))
			if(confirm && confirm.toUpperCase() === 'YES') {
				config = yield this.setup_slack(config)
			}

			//review and confirm
			console.log( chalk.yellow('Lets review:') );
			Object.keys(config).forEach( key => {

				console.log( chalk.yellow('\t' + key + ' : ' + JSON.stringify(config[key]) ))
			});

			confirm = yield prompt( chalk.yellow.bold('Write configuration [yes/NO]'));
			if(confirm.toUpperCase() !== 'YES') {
				console.log( chalk.red.bold('--ABORTED') );
				process.exit();
			}

			yield this._writeConfiguration(config);
			console.log( chalk.green.bold('--SUCCESS') );

			confirm = yield prompt( chalk.yellow.bold('Clone all packages on the verdaccio server to the development directory [YES/no]') );
			if( !confirm || confirm.toUpperCase() === 'YES' ){
				yield this.cloneAll({ exit: false });
			}


		}.bind(this) );
	}

	/**
	 * Configure the slack module.
	 * 
	 * @param {Object} config a configuration object (as retrieved from pielDT's RC), will use the instance config if not provided.
	 * @param {Boolean} write write to the configuration, default false.
	 * 
	 * @returns {Object} config the updated config, including the slack options.
	 */
	setup_slack( config, write ) {

		return co( function*() {

			const {yellow, cyan, red, green} = chalk;

			config = config || JSON.parse( JSON.stringify(this._config));
			write = write === undefined ? false : write;

			console.log(yellow('PielDT messages a channel in your slack channel via a hook created in Slack Channel admin.'))
			let slack_hook = yield prompt(yellow('Slack Hook:'));
			config.slack_enabled = true;
			config.slack_hook = slack_hook;

			if(write) {

				yield this.listConfig(config);
				let confirm = yield prompt(yellow('Continue [YES/no]'));
				if(!confirm || confirm.toUpperCase() === 'YES') {
					yield this._writeConfiguration(config);
					console.log( green.bold('--SUCCESS') );
				} else {
					console.log( red.bold('-- configuration aborted by user request.'));
				}
			}
			return config;

		}.bind(this))
	}


	/**
	 * List an argued configuration object, or the instance config.
	 * 
	 * @param {Object} config an arguable config, useful for listing a config out to the user before saving it, default Instance config.
	 * 
	 * @returns {void}
	 */
	listConfig( config ) {

		const listTree = function( obj, depth, itr, out ) {

			itr = itr || false;
			depth = depth || 1;
			out = out || '';
			let depthCnt = depth;

			Object.keys(obj).forEach( (key,i) => {

				while(depthCnt) { out += '  '; depthCnt--; }
				if(typeof obj[key] === 'object' && typeof obj[key].constructor === 'function') {
					out += chalk.yellow.bold(key + ':\n');
					out += listTree(obj[key],depth+1,true);
				} else {
					out += chalk.yellow.bold(key + ':')
					out += chalk.yellow(' ' + obj[key]);
				}

			});

			if(!itr){
				console.log(out);
			}
			return out;
		}

		return co( function*() {

			yield this._canExecuteSafely();
			config = config === undefined ? this._config : config;

			console.log(chalk.cyan('PDT Configuration:'));
			Object.keys(config).sort().forEach( key => {
				if(typeof config[key] === 'object' && typeof config[key].constructor === 'function') {
					console.log( chalk.yellow.bold(key + ':') );
					listTree(config[key]);
				} else {
					console.log( chalk.yellow.bold(key + ':') + ' ' + chalk.yellow(config[key]) );
				}
			})

		}.bind(this))
		.catch( err => { console.log('o...', err )});
	}

	/**
	*
	* Set a configuration variable
	*
	* @param {object} args The argument Object
	* @param {string} args.variable the config variable being setConfigVariable
	* @param {string|number} args.value = the value being set.
	**/
	setConfigVariable( args ) {

		return co(function*() {

			const {variable,value} = args;

			let config = JSON.parse( JSON.stringify( this._config ));
			config[variable] = value;

			yield this._writeConfiguration(config);
			yield this.listConfig();


		}.bind(this));
	}

	/**
	*
	* Set up a template for clonging.
	*
	* @param {object} args The argument object.
	* @param {string} args.name the name of the template
	*
	**/
	setupTemplate() {

		return co( function*() {

			let config = JSON.parse( JSON.stringify( this._config ) );
			config.templates = config.template || {};

			let exists,
				initial_dir,
				project_location;

			initial_dir = process.cwd();

			console.log(chalk.yellow( 'Creating new template `'+ name + '`' ));
			exists = prompt( chalk.cyan('Do you have an existing project you want to templatize [YES/no]'));
			if(!exists || exists.toUpperCase() !== 'YES') {
				project_location = yield prompt( chalk.cyan('Project location [' + initial_dir + ']:'));
				project_location = project_location || initial_dir;
			}

			//go to the templates directory

			//create new repository


		}.bind(this))
	}

	/**
	*
	* Unset a configuration variable
	*
	* @param {object} args The argument Object
	* @param {string} args.variable the config variable being setConfigVariable
	**/
	unsetConfigVariable( args ) {

		return co(function*() {

			const {variable} = args;

			let config = JSON.parse( JSON.stringify( this._config ));
			delete config[variable];

			yield this._writeConfiguration(config);
			yield this.listConfig();


		}.bind(this));
	}

	/**
	*
	* Get packgae information, and when argued, depdent package information as well (default: private sinopia server only but see options for more)
	*
	* @param {object} args the argument object
	* @param {string} args.name the name of the package to gain information for.
	* @param {boolean} args.recursive get information for dependents as well, default false.
	* @param {boolean} args.locally only include packages locally installed
	* @param {boolean} args.global include everything local, in a private registry, and the global npm registry.
	*
	**/
	packageInformation( args ) {

		return co( function*() {

			const {name} = args;
			const {yellow, red} = chalk;

			const isLocal = yield this.pkg.isLocal(name);
			const isLinkable = yield this.pkg.isLinkable(name);
			const linkLocation = yield this.pkg.linkLocation(name);

			console.log( yellow.bold('Package: ' + name ));
			console.log();
			console.log( yellow.bold('  In local development: ') + yellow(isLocal? 'YES' : 'NO') );
			console.log( yellow.bold('  Is linkable: ') + yellow(isLinkable ? 'YES' : 'NO') );
			console.log( yellow.bold('  Link Location: ') + yellow(linkLocation ? linkLocation : 'N/A') );
			console.log();

			const packages = yield this.pkg.getRemotePackages({config:true});
			console.log(yellow.bold('Downstream users:'));
			const dependentPackages = packages.filter( pkg => {
				const {dependencies, devDependencies} = pkg;
				if(dependencies && dependencies[name]) return true;
				if(devDependencies && devDependencies[name]) return true;
			});

			if(!dependentPackages.length) {
				console.log(yellow('  - none'));
			}

			let versions = [];

			dependentPackages.forEach( pkg => {

				console.log(yellow.bold('  ' + pkg.name + ':'));
				const {dependencies, devDependencies} = pkg;
				if(dependencies && dependencies[name]) {
					console.log(yellow('    dependency version:' + dependencies[name]));
					if( versions.indexOf(dependencies[name]) === -1) versions.push(dependencies[name]);
				}

				if(devDependencies && devDependencies[name]) {
					console.log(yellow('    development dependency version:' + devDependencies[name]));
					if( versions.indexOf(devDependencies[name]) === -1) versions.push(devDependencies[name]);
				}
			})

			if(versions.length > 1 ){
				console.log(red.bold('***WARNING*** package is being used ' + versions.length + ' times. it is recommended that you unify to one version.'))
			}

		}.bind(this));
	}

	/**
	 * 
	 * Interractively publish a package update through a user step-by-step CLI; this includeds the option to update downstream packages.
	 * 
	 * @param {Object} args the argument object
	 * @param {String} args.name the name of the package to publish
	 * @param {Boolean} args.project indicate the package is a project, default false.
	 * 
	 * @returns {void}
	 */
	publish( args ) {

		return co( function*() {

			yield this._canExecuteSafely();

			const {name, project, force, message} = args;
			const {dev_directory, project_directory} = this._config;
			const {yellow, cyan, red, green} = chalk;
			const initial_dir = process.cwd();

			let {bump, depbump} = args;

			let confirm,
				dep_tree,
				collapsed_deps,
				manage_downstream;

			let user_message = '';
			let message_log = [];

			//set the bump types, default "PATCH" for the packgae and deps.
			bump = bump || 'PATCH';
			depbump = depbump || 'PATCH';

			//get the package version.
			const package_version = yield this.pkg.localVersion(name);

			//get the package version to transition to.
			const new_package_version = this.pkg.build_new_version(package_version, bump);

			user_message = 'Publishing ' + name + ' from version ' + package_version + ' to ' + new_package_version;
			message_log.push(user_message);
			console.log(yellow.bold(user_message));
			
			//check for uncommited changes.
			let hasChanges = yield this.pkg.hasChanges(name);
			if(hasChanges) {

				console.log(yellow('Package ' + name + ' has local, uncommited/changed files:'));
				let changedFiles = yield this.pkg.changedFiles(name);
				changedFiles.forEach( file => {
					console.log(yellow('  - ' + file ));
				})
				confirm = yield prompt(cyan('Commit these changes now [YES/no]'));
				if(!confirm || confirm.toUpperCase() === 'YES') {

					let message = yield prompt(cyan('Commit message [NONE]:'));
					yield this.pkg.commit({ name: name, message: message });
				}
				console.log();
			}

			//check for commits.
			let hasCommits = yield this.pkg.hasCommits(name);
			if(!hasCommits){
				confirm = yield prompt( yellow('Package ' + name + ' has no commits, proceed with only a version bump [YES/no]'));
				if(confirm && confirm.toUpperCase() === 'NO'){
					console.log(yellow.bold('-- aborted by request'));
					process.exit();
				}
			}

			yield this.pkg.bumpVersion({name: name, version: bump});
			yield this.pkg.commit({ name: name, message: 'PDT Publish - version bumped from ' + package_version + ' to ' + new_package_version  });

			let commits = yield this.pkg.getUnpushedCommits({ name: name, lite: true });
			user_message = 'Publishing the following commits:';
			console.log(yellow(user_message));
			message_log.push(user_message);
			commits.forEach( commit => {
				user_message = commit;
				message_log.push( '\t' + user_message);
				console.log(yellow(commit));
			})

			confirm = yield prompt(cyan.bold('Continue [YES/no]'));
			if(confirm && confirm.toUpperCase() === 'NO'){
				console.log(yellow.bold('-- aborted by request'));
				process.exit();
			}

			yield this.pkg.pushToRepository({ name: name });
			yield this.pkg.publishToRegistry({ name: name });

			console.log(green.bold('Success'));


			//confirm the user wants to manage the downstream packages for this publish, and set true if so.
			confirm = yield prompt( cyan('Also update downstream packages [YES/no]'));
			if(!confirm || confirm.toUpperCase() === 'YES' ) {

				console.log();
				dep_tree = yield this.pkg.getDependentPackages({name: name, tree: true});
				collapsed_deps = yield this.pkg.collapseDepdencyTree({deps: dep_tree});
				this.pkg.logDownstreamDependents(name, dep_tree);
				manage_downstream = true;
			}
			else {
				manage_downstream = false;
			}

			if(manage_downstream && collapsed_deps.length) {

				console.log();
				message_log.push('');
				message_log.push('Updated downstream dependencies:')
				//ensure all downstream packages are at, or in front of the remote head.
				collapsed_deps.some( dep => {

					if(dep.hasNewerVersion) {
						console.log(red.bold('Refusing to continue, dependency ' + dep.name + ' has unfetched commits, do a git pull first.'));
						console.log(dep);
						process.exit();
					}
					return false;
				});


				//attach the target versions.
				collapsed_deps = collapsed_deps.map( dep => {

					if(dep.local)
						dep.target_version = this.pkg.build_new_version(dep.local_version, 'PATCH');

					//we give it the local version when we clone it later.

					return dep;
				})


				for(var i=0; i < collapsed_deps.length; i++ ) {

					let dep = collapsed_deps[i];

					console.log();
					console.log(yellow.bold('Downstream dependency ' + dep.name));
					message_log.push('\t' + 'Downstream dependency: ' + dep.name)
					if(!dep.local) {

						console.log(yellow('Package ' + dep.name + ' not local -- temporarily cloning for this operation.'));

						//clone it local.
						this.pkg.clone({name: dep.name, project: false});
						dep.unclone = true;
						dep.local_version = yield this.pkg.localVersion(dep.name);
					}
					else {

						if(dep.hasChanges){

							if(hasChanges) {

								console.log(yellow('Downstream package ' + dep.name + ' has local, uncommited/changed files:'));
								let changedFiles = yield this.pkg.changedFiles(dep.name);
								changedFiles.forEach( file => {
									console.log(yellow('  - ' + file ));
								})
								console.log(yellow('tip: You can manually resolve this before proceeding with "no".'));
								confirm = yield prompt(cyan('Commit these changes now [YES/no]'));
								if(!confirm || confirm.toUpperCase() === 'YES') {

									let message = yield prompt(cyan('Commit message [NONE]:'));
									yield this.pkg.commit({ name: dep.name, message: message });
								}
								console.log();
							}

						}

					}

					let manifest = collapsed_deps.concat([{name: name, target_version: new_package_version }]);

					yield this.pkg.bumpVersion({ name: dep.name, version: 'PATCH', manifest: manifest });
					yield this.pkg.commit({ name: dep.name, message: 'PDT Publish - version bumped from ' + dep.local_version + ' to ' + dep.target_version  });

					let commits = yield this.pkg.getUnpushedCommits({ name: dep.name, lite: true });
					console.log(yellow('Publishing the following commits:'));
					commits.forEach( commit => {
						message_log.push('\t\t' + commit);
						console.log(yellow(commit));
					});

					message_log.push('');

					yield this.pkg.pushToRepository({ name: dep.name });
					yield this.pkg.publishToRegistry({ name: dep.name });
				}

			}

			if( this._config.slack_enabled) {
				yield this.slack.message({ text: message_log.join('\n') });
			}

			//console.log(JSON.stringify(collapsed_deps, null, 2));

		}.bind(this))
		.catch( err => { console.log('err', err )});
	}

	/**
	* publish the
	* @param {object} args the argument Object
	* @param {string} args.name the component name which will have its depdencies bumped;
	* @param {string} args.release the type of release 'MAJOR', 'MINOR', 'PATCH' , default PATCH.
	* @param {boolean} args.force bump all depdent packages in the private registry server without prompt.
	**/
	bumpDependenciesVersions( args ) {

		return co( function*() {

			const {name, force} = args;

			let {release} = args;
			release = release || 'PATCH';

			let version = yield this._getDependencyVersion(name);
			console.log( 'GETTIN VERSION FOR ' + name + ' --- ' + version);

			let splVersion, newVersion;

			switch( release.toUpperCase() ) {

				case 'MAJOR':
					splVersion = version.split('.');
					splVersion[0]++;
					newVersion = splVersion.join('.');
					break;

				case 'MINOR':
					splVersion = version.split('.');
					splVersion[1]++;
					newVersion = splVersion.join('.');
					break;

				case 'PATCH':
					splVersion = version.split('.');
					splVersion[2]++;
					newVersion = splVersion.join('.');
					break;

				default:
					console.log( chalk.red.bold( 'Refusing to continue - cannot patch depdendency with ' + release + ' expecting [MAJOR/MINOR/PATCH]:') );
					process.exit();

			}

			let selectedDependencies = [];

			let selectDependency = function*(pkgname) {

				const pkgversion = yield this._getPackageVersion(pkgname);
				let update;
				if(!force) {

					update = yield prompt( chalk.cyan('Update ' + pkgname + '/' + pkgversion + ' to use ' + name + '/' + newVersion + ' [YES/no]:') );
					update = !update || update.toUpperCase() === 'YES';
				} else {

					update = true;
				}

				if(update){

					selectedDependencies.push({
						name: pkgname,
						current_version: pkgversion,
						new_version: newVersion
					});

					console.log('.......>', selectedDependencies);
				}

			}.bind(this);

			const dependencies = yield this._getDependencies(name, {recursive: true});
			for(var i=0; i < dependencies.length; i++ ){

				yield selectDependency(dependencies[i].name);
			}

			yield selectedDependencies.map( dep => {

				return co( function*() {

					const {name, current_version, new_version} = dep;

					let contents = yield this._getPackageContents(name);
					let package_version = contents.version;

					contents.version = new_version;


					console.log('=----->', name + ' ' + current_version + '-->' + contents.version + ' ::: package has it at ' + package_version );

				}.bind(this))
			})

		}.bind(this));

	}

	watch( args ) {

		return co( function*() {

			yield this._canExecuteSafely();

			const {dev_directory} = this._config;
			const {action} = args;

			console.log(chalk.green.bold('Watching ' + dev_directory ));
			console.log(chalk.green.bold('Actions:'));
			action.forEach( act => {
				console.log(chalk.green('\t- ' + act));
			});

			let func = function(evt,name) {

				switch( evt ) {

					case 'error':
						console.log('OHHH I GOT AN ERRORARGE', evt );
						break;

					case 'update':

						const {dev_directory} = this._config;

						let forwardDirectory = name.replace(dev_directory + '/', '');
						let component = forwardDirectory.split('/')[0];

						let initial_directory = process.cwd();
						let component_directory =  path.join(dev_directory, component);

						let isSourceDirectory = name.indexOf(component_directory + '/src') !== -1;
						if(!isSourceDirectory) {
							return;
						}

						process.chdir(component_directory);

						console.log(chalk.green.bold('compiling ' + component + ' at ' + process.cwd()));
						let execution = execSync('yarn run compile',{ stdio: 'inherit' });
						console.log(chalk.green.bold('--SUCCESS'));
						process.chdir(initial_directory);

				}


			}.bind(this);

			let directories = fs.readdirSync(dev_directory);
			for(var i=0; i < directories.length; i++ ) {

				let comp = directories[i];
				watch( path.join(dev_directory, comp, 'src'), { recursive: true }, func);
			}
		}.bind(this));
	}

	/**
	*
	* @param {object} args The argument object
	* @param {string} args.url the url to clone
	* @param {boolean} args.project clone as a project and to the project directory configured, otherwise as a component and to the coimponent directory.
	* @param {boolean} args.install install the component once cloned, default : false.
	* @param {boolean} args.link link all dependencies found in development, default : false.
	* @param {boolean} args.exit exit the process upon completion, default true.
	**/
	clone( args ){

		return co( function*() {

			yield this._canExecuteSafely();

			const {url} = args;

			let {exit, install, link, project} = args;

			exit = exit === undefined ? true : exit;
			project = project === undefined ? false : project;
			install = install = undefined ? false : install;
			link = link === undefined ? false : link;

			let treated_url = url;

			const {dev_directory, project_directory, use_ssh_id, ssh_id} = this._config;

			if(use_ssh_id) {
				treated_url = treated_url.indexOf('@') === -1
					? treated_url
					: treated_url.replace( url.split('@')[1].split('/')[0].split(':')[0], ssh_id);
			}

			let project_name = treated_url.split('/');
			project_name = project_name[project_name.length - 1].split('.')[0];

			/*find the module in sinopia*/
			let initial_directory = process.cwd();
			let target_directory = project ? project_directory : dev_directory;
			let clone_type = project ? 'Project' : 'Component';


			//clone
			console.log( chalk.green.bold('Cloning ' + clone_type + ' to ' + target_directory + '/' + project_name ) );
			process.chdir(target_directory);
			execSync('git clone ' + treated_url, { stdio: 'inherit' });
			console.log();
			console.log();

			//install
			if(install) {
				console.log(chalk.green.bold('Installing ' + clone_type + ' to ' + target_directory  + '/' + project_name ));
				process.chdir(path.join(target_directory, project_name));
				execSync('yarn install', { stdio: 'inherit' });
				process.chdir(target_directory);
			}

			if(link) {
				console.log(chalk.green.bold('Linking development modules to ' + project_name ));
				yield this.linkDependency({
					dependency: 'all',
					target: project_name,
					project : project
				})
			}

			process.chdir(initial_directory);

			console.log( chalk.green.bold('---SUCCESS') );
			if(exit)
				process.exit();

		}.bind(this));

	}

	/**
	* @param {object} args The argument object
	* @param {boolean} args.install install the component once cloned, default false.
	* @param {boolean} args.link link all dependencies found in development, default false.
	* @param {boolean} args.replace if cloned, replace it with a fresh clone, default false.
	* @param {boolean} args.exit exit the process upon completion, default true.
	**/
	cloneAll( args ) {

		return co( function*() {

			yield this._canExecuteSafely();

			let {replace,exit} = args;
			exit = exit === undefinend ? true : exit;

			const npm_server = yield this._getNPMServer();
			console.log( chalk.yellow.bold('Retrieving Package list from ' + npm_server ));
			const packages = yield this._getSinopiaPackages();
			console.log( chalk.yellow.bold('Done.'));

			for( var i=0; i < packages.length; i++ ) {

				let pkg = packages[i];
				let confirm_replace = false;

				if(!pkg.repository) {

					console.log( chalk.red.bold('Package ' + pkg.name + ' has no repository information - skipping.'));
					console.log();
					continue;
				}

				let pkg_url = typeof pkg.repository === 'string'
					? pkg.repository
					: pkg.repository.url;

				if(!pkg_url) {

					console.log( chalk.red.bold('Package ' + pkg.name + ' has no repository url - skipping.'));
					console.log();
					continue;
				}

				let exists = yield this._packageExists(pkg.name);

				if(exists && !replace) {

					confirm_replace = yield prompt( chalk.cyan.bold(' --- package ' + pkg.name + ' already exists, replace [yes/NO]'));
					if(confirm_replace.toUpperCase() !== 'YES'){
						console.log( chalk.yellow('skipping component ' + pkg.name) );
						console.log();
						continue;
					}
				}

				console.log(chalk.cyan.bold('Cloning ' + pkg.name + '/' + pkg.version + ' to ' + this._config.dev_directory));

				this.clone({ url: pkg_url, exit: false });
				console.log();
			}

			console.log(chalk.green.bold('--- SUCCESS'));

			if(exit)
				process.exit(1);

		}.bind(this));
	}

	/**
	*
	* clone from a template, and adjust the package contents to match.
	*
	* @param {object} args The argument Object
	* @param {string} args.name The name for this new package/project.
	* @param {string} args.template the name of the template to use
	* @param {boolean} args.project the clone will perform to the project directory, default: false; package directory is used.
	* @param {boolean} args.link link all package this clone depends on and exist in  the packages directory.
	* @param {boolean} args.develop, expose this through npm link
	*
	**/
	cloneFromTemplate( args ) {

		return co( function*() {

			const {name, template, project, link, develop} = args;
			const {templates, project_directory, dev_directory} = this._config;

			if(!templates[template]) {
				console.log( chalk.red.bold('Aborted -- Template ' + template + ' does not exist in the pdt configuration, use `pdt config list` to see what templates are available.'));
				return;
			}

			const template_url = templates[template].url;
			const newpackage_url = yield this.repo_instance.create_repo({name: name});

			const initial_dir = process.cwd();
			const target_dir =  project ? project_directory : dev_directory;

			const treated_template_url = this._treatedURL(template_url);
			const treated_newpackage_url = this._treatedURL(newpackage_url);

			const templateSplit = treated_template_url.split('/');
			const template_dir = path.join( target_dir, templateSplit[templateSplit.length-1].split('.')[0]);

			const newPackageSplit = treated_newpackage_url.split('/');
			const newpackage_dir = path.join( target_dir, newPackageSplit[newPackageSplit.length-1].split('.')[0]);

			process.chdir(target_dir);

			console.log('template URL', treated_template_url)
			console.log('newpackage URL', treated_newpackage_url)

			console.log('template DIR', template_dir)
			console.log('new package DIR', newpackage_dir)

			console.log( chalk.green('Cloning repository for ' + name ));
			execSync('git clone ' + treated_newpackage_url, {stdio: 'inherit'});
			console.log();

			console.log( chalk.green('Cloning template repository for ' + template ));
			execSync('git clone ' + treated_template_url, {stdio: 'inherit'});
			console.log();

			process.chdir(template_dir);
			execSync('rm -rf .git', {stdio: 'inherit'});
			execSync('cp -r * ' + newpackage_dir, {stdio: 'inherit'});

			process.chdir(target_dir);
			execSync('rm -rf ' + template_dir, {stdio: 'inherit'});

			let contents = yield this._getPackageContents(name, project);

			contents.name = name;
			contents.version = '0.1.0';
			contents.repository.url = newpackage_url;
			yield this._writePackageContents(name, contents, project);

			process.chdir(newpackage_dir);
			execSync('git add -A .', {stdio: 'inherit'});
			execSync('git commit -m "Initial commit"', {stdio: 'inherit'});
			execSync('git push -u origin master', {stdio: 'inherit'});
			console.log();

			let toNPM = yield prompt(chalk.cyan('Publish to Package Registry (npm/sinopia/verdaccio) [YES/no]'))
			if(!toNPM || toNPM.toUpperCase() === 'YES') {
				execSync('npm publish', {stdio: 'inherit'})
			} else {
				console.log(chalk.yellow('Skipping Registry publish by user request.'));
			}
			console.log();

			let installNow = yield prompt(chalk.cyan('Install Package [YES/no]:'));
			if( !installNow || installNow.toUpperCase() === 'YES') {
				execSync('yarn install', {stdio: 'inherit'});
			} else {
				console.log(chalk.yellow('Skipping install by user request.'));
			}
			console.log();

			let linkDeps = yield prompt(chalk.cyan('Link all known dependencies now [YES/no]'));
			console.log('Not implemented yet. Link dependencies manually');

			process.exit();

		}.bind(this));

	}

	/**
	* Install a component - equivical to yarn install.
	*
	* @param {object} args the argument object
	* @param {string} args.name the name of the component to install.
	* @param {boolean} args.force force a REINSTALL, default is false.
	* @param {boolean} args.link link all components in development, default is false.
	* @param {boolean} args.exit exit the process on completion, else return , default is true;
	**/
	installComponent( args ) {

		return co(function*(){

			yield this._canExecuteSafely();

			args.force = args.force === undefined ? false : args.force;
			args.link = args.link === undefined ? false : args.link;
			args.exit = args.exit === undefined ? true : args.exit;

			const {name, force, link, exit} = args;
			const {dev_directory} = this._config;

			const initial_directory = process.cwd();
			const component_directory = path.join(dev_directory, name);
			const node_modules_directory = path.join(dev_directory, name, 'node_modules');

			if( !force && fs.existsSync(node_modules_directory) ) {
				let confirmed = yield prompt( chalk.cyan.bold('Package ' + name + ' seems to already be installed, would you like to reinstall it [yes/NO] ') );
				if( confirmed.toUpperCase() !== 'YES') {
					console.log(chalk.red.bold('---ABORTED'));
					console.log();
					if(exit) process.exit(1);
					else return;
				}
			}

			process.chdir(component_directory);
			console.log( chalk.green.bold('Installing ' + name + ' --- please be patient this can take a few moments (normally < 1 min)'))
			execSync('yarn install', { stdio: 'inherit'} );
			process.chdir(initial_directory);
			console.log( chalk.green.bold('---SUCCESS'));
			if(exit)
				process.exit(1);

			return { success: true };

		}.bind(this))
		.catch( err => { console.log( 'Unhandled error in PDT::installComponent', err )});
	}

	/**
	* Install all components in the dev_directory -- equivical to yarn install for each component.
	*
	* @param {string} args.name the name of the component to install.
	* @param {boolean} args.force force a REINSTALL, default is false.
	* @param {boolean} args.link link all components in development, default is false.
	**/
	installAllComponents( args ){

		return co(function*(){

			const packages = yield this._getLocalPackages();
			const {force,link,replace} = args;
			let confirm;


			console.log(chalk.green.bold('Installing all components --- please be patient this can take a while'));

			for(var i=0; i < packages.length; i++ ) {

				let pkg = packages[i];

				yield this.installComponent({ name: pkg, link: link, force: force, exit: false });

			}

			console.log(chalk.green.bold('--SUCCESS'));

		}.bind(this));
	}

	/**
	* Update a dependency in a module/component
	*
	* @param {object} args The argument object.
	* @param {string} args.name The name of the dependency to update
	* @param {string} args.pkg the package to update the dependency upon.
	* @param {string} args.version the version to update to, default 'latest'
	* @param {boolean} args.project the package is a project, default false.
	* @param {boolean} args.exit exit the process on completion., default true.
	* @param {boolean} args.force complete operation without prompt.
	**/
	updateDependency( args ) {

		return co( function*() {

			yield this._canExecuteSafely();

			const {name, pkg} = args;
			const {dev_directory, project_directory} = this._config;
			let {version, project, exit, force} = args;

			version = version === undefined ? '' : version;
			project = project === undefined ? false : project;
			exit = exit === undefined ? true : exit;

			let package_version = yield this._getDependencyVersion(pkg, name, project);
			package_version = package_version.replace(/\^/g, '');

			let remote_version = yield this._getRemotePackageVersion(name);

			if(package_version === remote_version) {
				console.log( chalk.yellow('Package ' + pkg + ' has the latest version ' + remote_version + ' and does not need updating'));
				if(exit) process.exit(1);
				else return true;
			}

			let dependencyType = yield this._getDependencyType(pkg, name, project);
			if( !dependencyType) {
				console.log( chalk.red.bold('Dependency ' + name + ' not found on package ' + pkg ));
				if(exit) process.exit(1);
				else return true;
			}

			let isDevDep = (dependencyType === 'developmental');
			let isLinked = yield this._dependencyLinked(pkg, name, project);

			let initial_directory = process.cwd();
			let target_directory = project
				? path.join(project_directory,pkg)
				: path.join(dev_directory,pkg);

			if(!force) {
				let confirm = yield prompt( chalk.cyan.bold('Upgrade Package:' + pkg + ' dependency ' + name  + ' ('+ package_version + ' ---> ' + remote_version + ' [linked:' + isLinked.toString() + ']) [YES/no]'));
				if(confirm && confirm.toUpperCase() !== 'YES') {
					console.log( chalk.red.bold('---ABORTED'));
					process.exit();
				}
			} else {
				console.log( chalk.yellow('Updating package ' + pkg + ' with dependency ' + name + '('+ package_version + ' ---> ' + remote_version + ' [linked:' + isLinked.toString() + '])') );
			}

			process.chdir(target_directory);
			if(isLinked) {
				let package_contents = yield this._getPackageContents(pkg, project);

				if(isDevDep)
					package_contents.devDependencies[name] = '^' + remote_version;
				else {
					package_contents.dependencies[name] = '^' + remote_version;
				}
				yield this._writePackageContents(pkg, package_contents,project);

			} else {

				let cmd = 'yarn add ' + name;
				if(isDevDep) cmd += ' --dev';
				//execSync( 'yarn remove ' + name, { stdio: 'inherit' });
				execSync( cmd , { stdio: 'inherit'});

			}

			console.log( chalk.yellow('Upgraded package ' + pkg + ' dependency ' + name + ' ' + package_version + ' -> ' + remote_version ));
			process.chdir(initial_directory);
			console.log(chalk.green('---SUCCESS'));
			console.log();
			if(exit)
				process.exit(1);

		}.bind(this))
		.catch( err => { console.log( 'Unhandled expection on updateDependency', err )});

	}

	/**
	* Update a dependency in all module/components.
	*
	* @param {object} args The argument object.
	* @param {string} args.pkg the package to update the dependency upon.
	* @param {string} args.version the version to update to, default 'latest'
	* @param {boolean} args.project the package is a project, default false.
	* @param {boolean} args.exit exit the process on completion., default true.
	**/
	updateDependencies( args ) {

		return co( function*() {

			const components = yield this._getLocalPackages();
			const {name, force} = args;

			for( var i=0; i < components.length; i++ ) {

				let pkg = components[i];

				//dont want to update itself
				if(name === pkg) continue;

				//check to make sure the component has the package as dependency.
				let componentHasPackaged = yield this._getDependencyType(pkg, name);
				if(!componentHasPackaged) continue;

				yield this.updateDependency({ name: name, pkg: pkg, exit: false, force: force });

			}
			process.exit();
		}.bind(this));

	}

	/**
	*
	* Update all depdencies for an argued package.
	*
	* @param {object} args the argument object.
	* @param {string} args.pkg the package to updated depdencies upon.
	* @param {boolean} args.project the package is a project, default false.
	* @param {boolean} args.force update everything without prompting, default false.
	**/
	updateAllDependencies( args ) {

		return co( function*() {

			const {pkg, project, force} = args;

			let dependencies = [];
			let list;
			let package_contents = yield this._getPackageContents(pkg, project);

			if(package_contents.dependencies) {
				list = Object.keys(package_contents.dependencies).map( dep => {
					return {
						name: dep,
						version: package_contents.dependencies[dep].replace(/\^/g, '')
					}
				});
				dependencies = dependencies.concat(list);
			}

			if(package_contents.devDependencies) {
				list = Object.keys(package_contents.devDependencies).map( dep => {
					return {
						name: dep,
						version: package_contents.devDependencies[dep].replace(/\^/g, '')
					}
				});
				dependencies = dependencies.concat(list);
			}

			console.log(chalk.green('Retrieveing remote dependency information for ' + pkg + ' ... this can take some time, please be patient'));
			let remote_versions = yield dependencies.map( dep => {
				return this._getRemotePackageVersion(dep.name);
			});

			dependencies = dependencies.map( (dep, i) => {
				dep.remote = remote_versions[i];
				return dep;
			})

			if(!force) {
				for(var i=0; i < dependencies.length; i++ ) {

					const dep = dependencies[i];

					if( dep.version === dep.remote ) {
						console.log( chalk.yellow('Skipping ' + dep.name + ' -- package is on the latest remote version'));
						dependencies[i].update = false;
					} else {
						let confirm = yield prompt( chalk.cyan('Update ' + dep.name + ' from ' + dep.version + ' to ' + dep.remote + ' [YES/no]: ') );
						dependencies[i].update = !confirm || confirm.toUpperCase() === 'YES'
					}
				}

			} else {

				dependencies = dependencies.map( dep => {
					dep.update = dep.version !== dep.remote;
					return dep;
				})

			}
			console.log();

			console.log(chalk.yellow.bold('Review package dependency updates for package ' +pkg+ ':'))
			dependencies.forEach( dep => {
				dep.update
					? console.log(chalk.yellow.bold('\tUPGRADING ' + dep.name) + chalk.yellow( ' ' + dep.version + '->' + dep.remote ))
					: console.log(chalk.yellow.bold('\tSKIPPING ' + dep.name) + chalk.yellow(' up to date.' ));
			});
			console.log();

			let confirm = yield prompt(chalk.cyan.bold('Proceed [yes/NO]:'));
			if(confirm && confirm.toUpperCase() === 'YES') {

				yield dependencies.map( dep => {

					return dep.update
						? this.updateDependency({
							name: dep.name,
							pkg: pkg,
							force: true,
							exit: false,
							project: project
						})
						: true;

				})
			}



		}.bind(this));

	}

	/**
	* TODO
	**/
	reconcileDependencies( args) {

		return co( function*() {


		}.bind(this));
	}

	/**
	* Link a dependent package to a target package ( eg. link componentbase to rmc)
	*
	* @param {object} args The argument object.
	* @param {string|array} args.dependency = the dependency, the package which will become the dependent of the target_directory
	* @param {string} args.target - the package which will receive pkg as a dependent - if argued "all", ALL packages in dev_directory will receive the dependent.
	* @param {boolean} args.project - the target is a project, default false.
	* @param {boolean} args.force - force, without prompting.
	* @param {boolean} args.exit - exit the process on failure, default false.
	**/
	linkDependency( args ) {

		return co( function*() {

			yield this._canExecuteSafely();

			const {dev_directory, project_directory} = this._config;

			const {target} = args;
			let {dependency, force, project, exit} = args;
			project = project === undefined ? false : true;
			exit = exit === undefined ? false : true;

			//get the local package list
			let components;
			if(!(dependency instanceof Array)) {
				if(dependency.toUpperCase() !== 'ALL') {
					dependency = [dependency];
				} else {

					let deps = yield this._getLocalPackages();
					dependency = [];
					for(var s=0; s < deps.length; s++ ){
						let d = deps[s];
						let has = yield this._packageHasDependency(target, d, project);
						if(has) dependency.push(d);
					}
				}

			}

			const initial_directory = process.cwd();

			force = force === undefined ? false : force;

			let pkg,
				i,
				exists,
				noExistList,
				installed,
				isDependency,
				isDependencyLinked,
				isPackageLinked,
				dependency_location,
				target_location;

			//ensure the target exists
			target_location  = yield this._getPackageDirectory(target, project);
			if(!target_location) {
				console.log(chalk.red.bold('The directory for the target could not be found'));
				if(exit) process.exit();
				else return false;
			}

			//ensure the depedency/ies exists
			noExistList = [];
			exists = true;
			for(i=0; i < dependency.length; i++) {

				let dep = dependency[i];
				let result = yield this._packageExists(dep);
				if(!result) {
					exists = false;
					noExistList.push(dep);
				}

			}

			if(!exists || noExistList.length ) {

				console.log(chalk.red.bold('ABORTING --- the following packages are not in the development directory and cannot be linked'))
				noExistList.reduce((c, dep) => {
					console.log(chalk.red.bold('  - ' + dep));
				});
				if(exit) process.exit();
				else return;
			}

			//
			for(i=0; i < dependency.length; i++) {

				let dep = dependency[i];
				isPackageLinked = yield this._packageLinkExists(dep);
				if(isPackageLinked) {

					let linkLocation = yield this._getPackageLinkLocation(dep);

					if(linkLocation !== (dev_directory + '/' + dep) ) {
						console.log( chalk.cyan('Package ' + dep + ' is linked outside of PDTs development directory [' + linkLocation + ']'));
						let confirmRelink = yield prompt( chalk.cyan(' ***RECOMMENDED IF USING PDT WORKFLOW*** Replace this link with PDTs [yes/NO]: '));
						if(confirmRelink && confirmRelink.toUpperCase() === 'YES') {

							//go to the location.
							let actualLocation = yield this._getPackageLinkLocation(dep);

							//unlnk it.
							process.chdir(actualLocation);
							execSync('yarn unlink');
							console.log( chalk.yellow('Unlinked ' + dep + ' at ' + actualLocation));

							//go to PDT versions location.
							let newLocation = path.join(dev_directory, dep);
							process.chdir(newLocation);
							execSync('yarn link');
							console.log( chalk.yellow('Linked ' + dep + ' at ' + newLocation));

						}

					}

				}

			}

			for(i=0; i < dependency.length; i++) {

				let dep = dependency[i];

				//ensure the dependency exists
				exists = yield this._packageExists(dep);
				if(!exists) {
					console.log(chalk.yellow.bold('Skipping package ' + dep + ' - does not exist in the development directory'));
					continue;
				}

				//ensure the dependency is in an installed state (this may not be necessary, but step at a time.)
				installed = yield this._packageInstalled(dep);
				if(!installed) {
					console.log(chalk.yellow.bold('Skipping package ' + dep + ' - not installed and therefore cannot link.'));
					continue;
				}

				//get the dependency location - we've already error handled a non-existance.
				dependency_location = yield this._getPackageDirectory(dep);

				//ensure component has dependency, if it doesnt pompt to add it.
				isDependency = yield this._packageHasDependency(target, dep, project);
				let addDependency = false;
				let addAsDev = false;

				if(!isDependency) {

					let confirmAdd = yield prompt(chalk.cyan.bold('Target package ' + target + ' does not have a dependency on ' + dep  + '. Add it [yes/dev/NO]:'));
					if( confirmAdd && (confirmAdd.toUpperCase() === 'YES' || confirmAdd.toUpperCase() === 'DEV') ) {

						addAsDev = confirmAdd.toUpperCase() === 'DEV'
						process.chdir(target_location);
						execSync('yarn add ' + dep + (addAsDev ? ' --dev' : ''),{ stdio: 'inherit' });
						process.chdir(initial_directory);
						console.log()

					} else {
						continue;
					}
				}

				//ensure that its not already linked.
				isDependencyLinked = yield this._dependencyLinked(target, dep, project);
				if(isDependencyLinked) {
					console.log(chalk.yellow('Skipping package ' + target + '... module ' + dep + ' is already linked.'));
					continue;
				}

				//link it.
				process.chdir(target_location);
				console.log( chalk.yellow('linking ' + dep + ' to ' + target ));

				let packageLinkExists = yield this._packageLinkExists(dep);
				if(!packageLinkExists) {
					console.log(chalk.green.bold('Found unlinked package in the development directory ----- fixing.'));
					yield this._linkPackage(dep);
				}

				execSync('yarn link ' + dep);
				process.chdir(initial_directory);

			}

		}.bind(this))
		.catch( err => {
			console.log('WRD...', err );
		})
	}

	unlinkDependency( args ) {

		return co( function*() {

			yield this._canExecuteSafely();

			const {name} = args;
			let {project} = args;

			const {dev_directory, project_directory} = this._config;

			const components = yield this._getLocalPackages();

			const initial_directory = process.cwd();

			let pkg,
				exists,
				installed,
				package_location;

			project = project === undefined ? false : true;

			exists = this._packageExists(name);
			if(!exists) {
				console.log(chalk.red.bold('ABORTED --- Package ' + name + ' does not exist in the development directory'));
				if(exit) process.exit();
				else return;
			}

			for(var i=0; i < components.length; i++) {

				pkg = components[i];
				exists = yield this._packageExists(pkg);
				if(!exists) {
					console.log(chalk.yellow.bold('Skipping package ' + name + ' - does not exist in the development directory'));
					continue;
				}

				installed = yield this._packageInstalled(pkg, project);
				if(!installed) {
					console.log(chalk.yellow.bold('Skipping package ' + name + ' - not installed and therefore cannot link.'));
					continue;
				}

				package_location = yield this._getPackageDirectory(pkg, project);

				process.chdir(package_location);
				execSync('yarn link ' + name, { stdio: 'inherit' });
				process.chdir(initial_directory);

			}

		}.bind(this));
	}

	linkInfo( args ) {

		return co( function*() {

			const {name} = args;

			let stat = {};
			stat.location = yield this._getPackageLinkLocation(name);
			stat.packages = [];

			let local_version = yield this._getPackageVersion(name);
			let remote_version = yield this._getRemotePackageVersion(name);

			console.log(chalk.yellow.bold(name + ':' ));
			console.log(chalk.green.bold('\tLocation:') + ' ' + chalk.green(stat.location));
			console.log(chalk.green.bold('\tLocal Version:') + ' ' + chalk.green(local_version) );
			console.log(chalk.green.bold('\tRemote Version:') + ' ' + chalk.green(remote_version) );
			console.log();

			console.log(chalk.yellow('Gathering remote dependency versions....'));
			console.log();

			/*make this a function... its pretty useful*/
			let versions = yield this._getDependecyVersions(name);
			let remote_versions = yield Object.keys(versions).map( pkg => {
				return this._getRemotePackageVersion(pkg);
			});
			let version_compare = Object.keys(versions).reduce( (c, pkg, i) => {
				c[pkg] = {
					current: versions[pkg],
					available: remote_versions[i]
				}
				return c;
			},{});
			/*snip*/

			Object.keys(version_compare).forEach( pkg => {
				console.log(chalk.yellow.bold(pkg + ':'));
				let detail = version_compare[pkg];
				console.log(chalk.yellow('\tlocal: ' + detail.current));
				console.log(chalk.yellow('\tremote: ' + detail.available));
				console.log();
			})

		}.bind(this))
	}

	allLinksInfo() {

		return co( function*() {

			const linked_packages = yield this._getLinkedPackages();

			let pkg;
			for(var i=0; i < linked_packages.length; i++ ) {

				pkg = linked_packages[i];
				yield this.linkInfo({name: pkg });
				console.log();
			}

		}.bind(this))
	}

	crosslink( args ) {

		return co( function*() {

			const packages = yield this._getLocalPackages();

			let target, dep;

			for( var i=0; i < packages.length; i++ ) {

				target = packages[i];
				yield this.linkDependency({ target: target, dependency: 'all' });
			}

		}.bind(this));
	}

	/* returns a configuration object, read from ~/.pdtrc */
	_configuration() {

		const rc_filename = os.homedir()  + '/.pdtrc';
		const configExists = fs.existsSync(rc_filename);

		if(!configExists) return false;
		const rc_contents = fs.readFileSync(rc_filename, 'utf-8');
		const parsed_contents = JSON.parse(rc_contents);

		return parsed_contents;
	}

	_canExecuteSafely() {

		return co( function*() {

			if(!this._hasRCConfig) {
				console.log( chalk.red.bold( 'Cannot execute, install Piel Dev Tools first. No configuration found.') )
				process.end();
			}

			const current_npm_registry = yield this._getNPMServer();
			const current_yarn_registry = yield this. _getYarnServer();
			const configured_registry = this._config.registry_url;

			if( configured_registry &&
				(current_yarn_registry !== current_npm_registry ) ||
				(current_yarn_registry !== configured_registry)
			){

				const {yellow} = chalk;

				execSync('npm set registry ' + configured_registry);
				execSync('yarn config set registry ' + configured_registry);

				const registry = yield this._getNPMServer();

				console.log(yellow('set registry to ' + registry ));
			}

		}.bind(this));

	}

	/**
	* Write the configuration to the users pdtrc file.
	**/
	_writeConfiguration( config ) {

		return co( function*() {

			let rc_filename = os.homedir()  + '/.pdtrc';
			fs.writeFileSync( rc_filename, JSON.stringify( config, null, 2));
			this._config = config;

		}.bind(this));
	}

	/**
	* Get the package.json contents as an object for an argued component
	*
	* @param {string} name the name of the package
	* @param {boolean} project the package is a project, default false
	* @return {object} the package.json contents.
	**/
	_getPackageContents( name, project ) {

		return co( function*() {

			yield this._canExecuteSafely()

			project = project || false;

			const {dev_directory, project_directory} = this._config;
			const package_location = path.join( project ? project_directory : dev_directory, name) + '/package.json';
			let contents = fs.readFileSync(package_location, 'utf8');
			return JSON.parse(contents);

		}.bind(this))
	}

	/**
	* Write contents as package.json on the argued package
	*
	* @param {string} name the name of the package to receive these new contents.
	* @param {object} contents the package contents in their object form.
	* @param {boolean} project package is a project, default false.
	**/
	_writePackageContents( name, contents, project ) {

		return co( function*() {

			yield this._canExecuteSafely()

			const {dev_directory, project_directory} = this._config;
			const package_location = path.join( project ? project_directory : dev_directory, name) + '/package.json';

			fs.writeFileSync( package_location, JSON.stringify( contents, null, 2));
			return true;

		}.bind(this))
	}

	/**
	*
	* Check to see if a package Exists.
	*
	* @param {string} name the name of the package to check for
	* @param {boolean} exitOnFail kill the process with an error message if package does not exit, default false.
	*
	**/
	_packageExists( name, project, exitOnFail ) {

		return co( function*() {

			yield this._canExecuteSafely();

			exitOnFail = exitOnFail === undefined ? false : exitOnFail;

			const {dev_directory, project_directory} = this._config;
			const root_dir = project ? project_directory : dev_directory;

			const package_location = root_dir + '/' + name;
			const exists = fs.existsSync(package_location);

			if(exitOnFail && !exists) {
				console.log( chalk.red.bold('Package ' + name + ' does not exist at ' + package_location ));
				console.log( chalk.red.bold('Aborted'));
				process.exit(1);
			}

			return exists;
		}.bind(this));

	}

	/**
	* Get the names of all linked packages.
	**/
	_getLinkedPackages() {

		return co( function*() {

			const linked_dir = path.join(os.homedir(), '.config', 'yarn', 'link');
			return fs.readdirSync(linked_dir);

		}.bind(this));
	}

	/**
	* Determine if a package is already linked.
	*
	* This is as dirty as sin being that there is no way to get either (a) the status of what you have linked via yarn.link (b) a config of where its getting its known links from .
	* I went through the yarn source but don't have the time to create a nice solution.
	* Therefore this simply attempts to link it, if it fails assume it is already linked somewhere, else unlink it and return true. DIRTY!
	*
	* @param {string} name the name of the package.
	* @return boolean, true if a link exists, else false.
	**/
	_packageLinkExists( name, exitOnFail ) {

		return co( function*() {

			const {dev_directory} = this._config;

			const initial_dir = process.cwd();
			const module_dir = path.join(dev_directory,name);
			const linked_dir = path.join(os.homedir(), '.config', 'yarn', 'link');
			const linked_components = fs.readdirSync(linked_dir);

			return !!linked_components.find( comp => { return comp === name });

		}.bind(this));
	}

	/**
	*
	* @param {string} name the name of the package
	* @param {boolean} exitOnFail kill the process on failure to find the package, default false.
	**/
	_getPackageLinkLocation( name, exitOnFail ) {

		return co( function*() {

			const {dev_directory} = this._config;

			exitOnFail = exitOnFail === undefined ? false : exitOnFail;

			const initial_dir = process.cwd();
			const module_dir = path.join(dev_directory,name);
			const linked_dir = path.join(os.homedir(), '.config', 'yarn', 'link');
			const linked_components = fs.readdirSync(linked_dir);

			let exists = !!linked_components.find( comp => { return comp === name });
			if(!exists) {
				if(exitOnFail) process.exit();
				return false;
			}

			return fs.realpathSync(linked_dir + '/' + name, 'utf8');

		}.bind(this));
	}

	_linkPackage( name, project ) {

		return co( function*() {

			yield this._canExecuteSafely();

			const {dev_directory, project_directory} = this._config;
			const package_directory = project ? project_directory : dev_directory;
			const initial_directory = process.cwd();
			const module_directory = path.join(package_directory,name);

			console.log(chalk.yellow('Exposing ' + (project ? 'Project' : 'Module') + ' ' + name + 'as a linkable module [' + module_directory + ']'));
			process.chdir(module_directory);
			execSync('yarn link');
			process.chdir(initial_directory);



		}.bind(this));
	}

	_unlinkPackage( name, project ) {

		return co( function*() {

			yield this._canExecuteSafely();

			const {dev_directory, project_directory} = this._config;
			const package_directory = project ? project_directory : dev_directory;
			const initial_directory = process.cwd();

			console.log(chalk.yellow('Exposing ' +(project ? 'Project' : 'Module')+ ' as a linkable module'));
			process.chdir(package_directory, name);
			execSync('yarn unlink');
			process.chdir(initial_directory);



		}.bind(this));
	}

	/**
	*
	* Check to see if a package is installed (ie. it has node_modules).
	*
	* @param {string} name the name of the package to check for
	* @param {boolean} exitOnFail kill the process with an error message if package is already installed, default false.
	*
	**/
	_packageInstalled( name, project, exitOnFail ) {

		return co( function*() {

			yield this._canExecuteSafely();

			exitOnFail = exitOnFail === undefined ? false : exitOnFail;

			const {dev_directory,project_directory} = this._config;

			const root_dir = project ? project_directory : dev_directory;
			const package_location = root_dir + '/' + name + '/node_modules';

			const exists = fs.existsSync(package_location);

			if(exitOnFail && !exists) {
				console.log( chalk.red.bold('Package ' + name + ' is already installed' ));
				console.log( chalk.red.bold('Aborted'));
				process.exit(1);
			}

			return exists;
		}.bind(this));
	}

	/**
	* Check to see if a package has a dependency
	*
	* @param {string} pkg the name of the package
	* @param {string} name the name of the dependency
	* @param {boolean} project the package is a project, default false.
	* @param {boolean} exitOnFail kill the process if it doesn't exist, default false.
	*/
	_packageHasDependency( pkg, name, project, exitOnFail ) {

		return co( function*() {

			yield this._canExecuteSafely();

			exitOnFail = exitOnFail === undefined ? false : exitOnFail;
			project = project === undefined ? false : project;

			const {dev_directory, project_directory} = this._config;
			const root_dir = project ? project_directory : dev_directory;
			const node_directory = path.join(root_dir, pkg, 'node_modules');
			const components = fs.readdirSync(node_directory);

			return !!components.find( compdir => { return compdir === name });
		}.bind(this));
	}

	/**
	* get the version of a package, as declared locally ( in your development directory)
	*
	* @param {string} name the name of the package to retrieve the version for.
	*
	* @return {string} the version.
	*/
	_getPackageVersion( name ) {

		return co( function*() {

			yield this._canExecuteSafely();

			const package_location = this._config.dev_directory + '/' + name + '/package.json';
			let package_contents;

			try {
				package_contents = fs.readFileSync(package_location, 'utf-8');
			} catch(e) {
				console.log(chalk.red.bold('package.json could not be found at ' + package_location ));
				console.log(chalk.red.bold('Aborted.'));
				process.exit(1);
			}

			const contents = JSON.parse(package_contents);

			return contents.version;

		}.bind(this))
	}

	/**
	* Get the version the remoteof a package, as declared on the remote service (NPM/Sinopia/Verdaccio).
	*
	* @param {string} name The name of the subject component.
	* @return the version or false if it isn't a dependenecy of the component.
	**/
	_getRemotePackageVersion( name ) {

		return co( function*() {

			let detail = execSync('npm view ' + name + ' --json');
			let contents = JSON.parse(detail.toString());
			return contents.version;
		}.bind(this))
	}

	/**
	* Get the installed directroy for  agiven package.
	*
	* @param {string} name the name of the package.
	* @return the packaged directroy.
	**/
	_getPackageDirectory(name, project, exitOnFail ) {

		return co( function*() {

			yield this._canExecuteSafely();

			exitOnFail = exitOnFail === undefined ? false : true;

			const {dev_directory, project_directory} = this._config;
			const root_dir = project ? project_directory : dev_directory;
			const package_dir = path.join(root_dir, name);

			let exists = fs.existsSync(package_dir);
			if(!exists) {
				console.log( chalk.red.bold('Package ' + name + ' was not located at [' + package_dir + ']') );
				if(exitOnFail) process.exit()
				else return false;
			}

			return package_dir;

		}.bind(this));

	}

	/**
	* Get packages that are depdendent on an argued package
	*
	* @param {string} name the package to find other packages that depend on it.
	* @param {string} project include projects that are dependent.
	* @param {object} options options Object
	* @param {boolean} options.recursive
	* @return {}
	**/
	_getDependencies( name, options ) {

		return co( function*() {

			const components = yield this._getLocalPackages();

			options = options || {};

			let {recursive} = options;
			recursive = !!recursive;

			let dependent_components = [];

			for(var i=0; i < components.length; i++) {

				let component = components[i];
				let has = yield this._packageHasDependency(component, name);

				if(has) {
					if(recursive) {
						let shim = { name: component, children : []};
						shim.children = yield this._getDependencies(component, options);
						dependent_components.push(shim);
					} else {
						dependent_components.push({ name: component });
					}
				}

			}

			return dependent_components;

		}.bind(this));
	}

	/**
	* Get the version of a dependency, given a component.
	*
	* @param {string} name The name of the subject component.
	* @param {string} dependency the name of the dependecy of which you want the version of.
	*
	* @return the version or false if it isn't a dependenecy of the component.
	**/
	_getDependencyVersion( name, dependency, project ) {

		return co( function*() {

			yield this._canExecuteSafely();

			const {dev_directory, project_directory} = this._config;
			const package_location = path.join(project ? project_directory : dev_directory, name) + '/package.json';
			let package_contents;

			try {
				package_contents = fs.readFileSync(package_location, 'utf-8');
			} catch(e) {
				console.log(chalk.red.bold('package.json could not be found at ' + package_location ));
				console.log(chalk.red.bold('Aborted.'));
				process.exit(1);
			}

			let contents = JSON.parse(package_contents);

			//populate for eval
			contents.dependencies = contents.dependencies || {};
			contents.devDependencies = contents.devDependencies || {};

			//dependency is not actually a dependency
			if( !contents.dependencies[dependency] && !contents.devDependencies[dependency] ) return false;

			return contents.dependencies[dependency] || contents.devDependencies[dependency];

		}.bind(this));
	}

	_getDependecyVersions( name ) {

		return co( function*(){

			const packages = yield this._getLocalPackages();

			let pkg,
				version,
				versions;

			versions={};

			for(var i=0; i <packages.length; i++) {

				pkg = packages[i];

				version = yield this._getDependencyVersion(pkg, name);
				if(!version) continue;

				versions[pkg] = version;

			}

			return versions;

		}.bind(this));
	}

	/**
	* Get whether a dependency is main or developmental in a package.
	*
	* @param {string} pkg The package to check the dependency against.
	* @param {string} name the name of the dependency Type (main or developmental).
	* @param {boolean} project the package is a project.
	* @param {boolean} exit exit the process if the dependency could not be found, default false.
	*
	* @return 'main', 'developmental' , or false if it simply does not exist.
	**/
	_getDependencyType( pkg, name, project, exit ) {

		return co( function*() {

			yield this._canExecuteSafely();

			const {dev_directory, project_directory} = this._config;
			const package_location = path.join( project ? project_directory : dev_directory, pkg ) + '/package.json';

			let package_contents;

			try {
				package_contents = fs.readFileSync(package_location, 'utf-8');
			} catch(e) {

				console.log(chalk.red.bold('package.json could not be found at ' + package_location ));
				console.log(chalk.red.bold('Aborted.'));
				if(exit)
					process.exit(1);
				else
					return false
			}

			let contents = JSON.parse(package_contents);

			if( contents.dependencies && contents.dependencies[name] ) return 'main';
			if( contents.devDependencies && contents.devDependencies[name] ) return 'developmental';

			return false;

		}.bind(this));
	}

	/**
	* get whether a depdency to an argued package exists as a symlink
	*
	*
	**/
	_dependencyLinked( pkg, name, project ) {

		return co( function*() {

			yield this._canExecuteSafely();

			const {dev_directory, project_directory} = this._config;

			const root_dir = project ? project_directory : dev_directory;

			const module_dir = path.join(root_dir, pkg, 'node_modules', name );

			if(!fs.existsSync(module_dir)) {
				console.log( chalk.red.bold('Package ' + pkg + ' does not have module ' + name + ' in the node_modules directory' ));
				process.exit();
			}

			const stat = fs.lstatSync(module_dir);
			return stat.isSymbolicLink();

		}.bind(this));
	}

	_packageLinked( name ) {

		return co( function*() {

		}.bind(this));

	}

	/**
	* convert a version string (eg. ^1.0.0) to its numeric form
	**/
	_numericVersion(version) {

	}

	/**
	* Get the currently npm configured registry.
	**/
	_getNPMServer() {

		return co( function*() {

			let ret = execSync('npm config get registry')
			return ret.toString().replace(/\n/g, '');
		}.bind(this));
	}

	_getYarnServer() {

		return co( function*() {

			let ret = execSync('yarn config get registry');
			return ret.toString().replace(/\n/g, '');
		}.bind(this));
	}

	/**
	* Get the packages on the sinopia server
	**/
	_getSinopiaPackages() {

		return new Promise( (resolve,reject) => {

			co( function*() {

				let registry_url, url;

				registry_url = yield this._getNPMServer();
				url = registry_url + '-/search/*';

				rp.get({ url: url, json: true })
					.then( packages => {
						resolve(packages);
					});

				}.bind(this));
		});
	}

	/**
	* Get the packages in the component directory
	*
	* @return {array} a list of components
	**/
	_getLocalPackages() {

		return co( function*() {

			const {dev_directory} = this._config;
			return fs.readdirSync(dev_directory);

		}.bind(this));
	}

	/**
	*
	* Get a treated url (replaces the location with an ssh_id ) eg git@bitbucket.org/team/project.git becomes git@myid/team/project.git.
	*
	**/
	_treatedURL( url ) {

		const {use_ssh_id, ssh_id} = this._config;

		let treated_url = url;
		if(use_ssh_id) {
			treated_url = url.indexOf('@') === -1
				? url
				: url.replace( url.split('@')[1].split('/')[0].split(':')[0], ssh_id);
		}

		return treated_url;
	}
}
