import co from 'co';
import prompt, { password } from 'co-prompt';
import chalk from 'chalk';

export default class BitbucketAPI {

	 constructor() {

	 }

	 initialize( config ) {

		 this._config = config;
	 }

	 setup( ) {

		 return co( function*() {

			 console.log('Bitbucket not implemented yet.  Dont worry! You will be prompted to complete steps until it is.')
			 return this._config;

		 }.bind(this));


	 }

	 /**
	 * Create a repository on Bitbucket
	 *
	 * @param {object} args the argument object.
	 * @param {string} args.name the name for the new repository
	 *
	 **/
	 create_repo( args ) {

		 return co( function*() {

			 const {name} = args;
			 console.log('Automated creation of Bitbucket repositories is not enabled yet. Manually create the repository `' + name + '` through the interface before continuing.' );
			 yield prompt('Repository ready? [YES/no]:');

			 let done = false;
			 let url, confirm;

			 while(!done) {

				 url = yield prompt('Paste the provided URL here: ');
				 console.log( chalk.yellow('Using ' + url ));

				 confirm = yield prompt('Correct? [YES/no]:');
				 if(!confirm || confirm.toUpperCase() === 'YES') {
					 done = true;
				 } else {
					console.log('lets try again..');
				}
			}

			return url;

		 }.bind(this));

	 }
 }
