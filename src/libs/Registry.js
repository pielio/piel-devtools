import co from 'co';
import {execSync} from 'child_process';

export default class Registry {

	constructor(config) {

		this._config = config;
	}

	/**
	* Get the current configured registry.
	*
	* @return {string} the current registry, from the instance config.
	**/
	configured_registry() {

		return co( function*() {

			const url = this._config.registry_url;
			return url.substr(url.length - 1) === '/'
				? url.substr(0, url.length-1)
				: url;

		}.bind(this));
	}

	/**
	* Get the current registry in use by the runtime.
	*
	* @return {string} the curren tregistry, obtained from yarn.
	**/
	current_registry() {

		return co( function*() {

			let response = execSync('yarn config get registry');
			let url = response.toString().replace(/(?:\r\n|\r|\n)/g, '');
			return url.substr(url.length - 1) === '/'
				? url.substr(0, url.length-1)
				: url;

		}.bind(this));
	}

	/**
	* Check to see the runtime is using the configured registry.
	*
	* @return {boolean} true/false.
	**/
	using_configured_registry() {

		return co( function*() {

			const current = yield this.current_registry();
			const configured = yield this.configured_registry();

			return current === configured;
		}.bind(this));

	}

}
