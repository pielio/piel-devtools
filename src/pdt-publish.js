#!/usr/bin/env node

import program from 'commander';
import fs from 'fs';
import co from 'co';
import PielDT from './libs/PielDT';
program
	.arguments('<name>')
	.action( ( name, commander ) => {

		co( function*() {

			const Instance = new PielDT();
			yield Instance.publish({
				name: name
			});
			process.exit(1);
		})
		.catch( err => { })

	});

program.on('--help', function(){
	console.log('  Examples:');
	console.log('');
	console.log('    $ pdt publish mypackage' );
	console.log('     - bumps the package version (eg. v1.0.1 to v1.0.2)');
	console.log('     - prompts to rollup any uncommited changes in with the version bump');
	console.log('     - warns if there are no commits resident (ie. only doing a version bump)');
	console.log('     - prompts to update any downstream package found in the private repo');
	console.log('     - interractively steps through updating the downstream packages, including any uncommited updates etc etc.');
});

program.parse(process.argv);
