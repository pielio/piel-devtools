#!/usr/bin/env node

import program from 'commander';
import fs from 'fs';
import co from 'co';
import PielDT from './libs/PielDT';

program
	.arguments('<name>')
	.option('-r, --recursive', 'get information about depedencies')
	.action( ( name, commander ) => {

		const options = {
			name: name,
			recursive: !!program.recursive
		};

		co( function*() {

			const PDT = new PielDT();
			yield PDT.packageInformation(options);
			process.exit();

		});

	});

program.parse(process.argv);

program.on('--help', function(){
	console.log('  Examples:');
	console.log('');
	console.log('    $ pdt info mypackage');
	console.log('    $ pdt link mypackage -r (provides information all the way down the dep tree for modules found in your verdaccio server)');
	console.log('');
});
