#!/usr/bin/env node

import program from 'commander';
import fs from 'fs';
import co from 'co';
import PielDT from './libs/PielDT';

co( function*() {

	const PDT = new PielDT();
	yield PDT.setup();
	process.exit();

}).catch( err => {
	console.log('Setup Failed:::', err );
});
