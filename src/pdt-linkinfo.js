#!/usr/bin/env node

import program from 'commander';
import fs from 'fs';
import co from 'co';
import PielDT from './libs/PielDT';
import chalk from 'chalk';

program
	.arguments('<package name>')
	.action( ( name, commander ) => {

		co( function*() {

			const Instance = new PielDT();

			if(name.toUpperCase() === 'ALL') {
				yield Instance.allLinksInfo();
			} else {
				yield Instance.linkInfo({name: name});
			}

			process.exit();

		})
		.catch( err => { console.log('EDT linkinfo failed with: ', err )});

	});

program.on('--help', function(){
	console.log('  Examples:');
	console.log('');
	console.log('    $ pdt linkinfo mypackage');
	console.log('');
});

program.parse(process.argv);
