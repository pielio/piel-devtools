#!/usr/bin/env node

import program from 'commander';
import fs from 'fs';
import co from 'co';
import PielDT from './libs/PielDT';
program
	.arguments('<action> [variable] [value]')
	.action( ( action, variable, value, commander ) => {

		return co( function*() {

			const Instance = new PielDT();

			switch( action ) {

				case 'list':
					yield Instance.listConfig();
					process.exit(1);

				case 'set':
					yield Instance.setConfigVariable({ variable: variable, value: value});
					process.exit(1);

				case 'unset':
					yield Instance.unsetConfigVariable({ variable: variable });
					process.exit(1);

				case 'add':
					switch( variable ) {

						case 'template':
							yield Instance.setupTemplate({ name: value });
							process.exit(1);

						default:
							console.log('PDT config add - unknown type ' + variable );
							process.exit(1);
					}

				case 'remove':
					switch( variable ) {

						case 'template':
							yield Instance.deleteTemplate({ name: value });
							process.exit(1);

						default:
							console.log('PDT config add - unknown type ' + variable );
							process.exit(1);
					}

				case 'feature':
					switch(variable) {
						case 'slack':
							yield Instance.setup_slack(null, true);
							process.exit(1);
					}
				case 'features':
					console.log('PDT supported features: slack');

				default:
				 	console.log('PDT config -- unknown action ' + action );
					process.exit(1);
			}
		})
		.catch( err => { console.log('pdt-config exception error:', err); })

	});

program.on('--help', function(){
	console.log('  Examples:');
	console.log('');
	console.log('    $ pdt config list (List the pdf config)');
	console.log('    $ pdt config set <option> <value> (set an value for an option)');
	console.log('    $ pdt config unset <option> (removes an option from the configuration)');
	console.log('    $ pdt config features (list available feature modules)');
	console.log('    $ pdt config feature <feature> (run the configuration for an argued feature module)');
	console.log('    $ pdt config add template <mygitrepo> (add a known starter kit --- experimental!)');
	console.log('    $ pdt config remove template <mygitrepo> (remove a known starter kit --- experimental!)')
	console.log('');
});
program.parse(process.argv);
