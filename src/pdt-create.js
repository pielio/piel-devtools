#!/usr/bin/env node

import program from 'commander';
import fs from 'fs';
import co from 'co';
import PielDT from './libs/PielDT';
program
	.arguments('<name> <template>')
	.option( '-p, --project', 'Create the cloned template in the project directory')
	.option( '-l, --link', 'after creation, link everything found in the packages directory to it')
	.option( '-d, --develop', 'set the cloned template to a development mode - ie. yarn link <name>' )
	.action( ( name, template, commander ) => {

		co( function*() {

			const {project,link,develop} = program;

			let args = {
				name: name,
				template: template,
				project: !!project,
				link: !!link,
				develop: !!develop
			}

			const Instance = new PielDT();

			Instance.cloneFromTemplate(args);
		})
		.catch( err => {
			console.log('PDT::create failed with an exception error', err );
		})

	});

program.on('--help', function(){
	console.log('  Examples:');
	console.log('');
	console.log('    $ pdt create mypackage react-component  (clones the "react-component" template to the packages directory as "mypackage", updating the package.json)');
	console.log('    $ pdt create myproject react-project -p (clones the "react-project" template to the project directory as "myproject", updating the package.json)');
	console.log('    $ pdt create myproject react-project -pl (clones the "react-project" template to the project directory as "myproject", updating the package.json, AND linking every package found in the package directory)');
	console.log('');
});

program.parse(process.argv);
