#!/usr/bin/env node

import program from 'commander';
import fs from 'fs';
import co from 'co';
import PielDT from './lib/PielDT';

program
	.arguments('<name>')
	.option('-p, --project', 'install a project, not a component')
	.option('-f, --force', 'install the component without interruption')
	.option('-r, --replace', 'replace the install component if it exists')
	.option('-l, --link', 'link all dependencies found in development')
	.action( ( name, commander ) => {

		const {project, force, link, replace} = program;
		const options = {
			name: name,
			project: !!project,
			force: !!force,
			replace: !!replace,
			link : !!link
		};

		co( function*() {

			const PDT = new PielDT();

			if(options.name.toLowerCase() === 'all') {
				yield PDT.installAllComponents(options);
			} else {
				yield PDT.installComponent(options);
			}

		});
	});

program.on('--help', function(){
	console.log('  Examples:');
	console.log('');
	console.log('    $ pdt install mypackage (clones the package to your packages directory)');
	console.log('    $ pdt create mypackage -l (clones the package to your packages directory, and links any depdencies found in the pakcage directory)');
	console.log('');
});

program.parse(process.argv);
