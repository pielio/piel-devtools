#!/usr/bin/env node
'use strict';

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _co = require('co');

var _co2 = _interopRequireDefault(_co);

var _PielDT = require('./libs/PielDT');

var _PielDT2 = _interopRequireDefault(_PielDT);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_commander2.default.arguments('<name>').action((name, commander) => {

	(0, _co2.default)(function* () {

		const Instance = new _PielDT2.default();
		yield Instance.publish({
			name: name
		});
		process.exit(1);
	}).catch(err => {});
});

_commander2.default.on('--help', function () {
	console.log('  Examples:');
	console.log('');
	console.log('    $ pdt publish mypackage');
	console.log('     - bumps the package version (eg. v1.0.1 to v1.0.2)');
	console.log('     - prompts to rollup any uncommited changes in with the version bump');
	console.log('     - warns if there are no commits resident (ie. only doing a version bump)');
	console.log('     - prompts to update any downstream package found in the private repo');
	console.log('     - interractively steps through updating the downstream packages, including any uncommited updates etc etc.');
});

_commander2.default.parse(process.argv);