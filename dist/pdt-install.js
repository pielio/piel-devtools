#!/usr/bin/env node
'use strict';

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _co = require('co');

var _co2 = _interopRequireDefault(_co);

var _PielDT = require('./lib/PielDT');

var _PielDT2 = _interopRequireDefault(_PielDT);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_commander2.default.arguments('<name>').option('-p, --project', 'install a project, not a component').option('-f, --force', 'install the component without interruption').option('-r, --replace', 'replace the install component if it exists').option('-l, --link', 'link all dependencies found in development').action((name, commander) => {

	const { project, force, link, replace } = _commander2.default;
	const options = {
		name: name,
		project: !!project,
		force: !!force,
		replace: !!replace,
		link: !!link
	};

	(0, _co2.default)(function* () {

		const PDT = new _PielDT2.default();

		if (options.name.toLowerCase() === 'all') {
			yield PDT.installAllComponents(options);
		} else {
			yield PDT.installComponent(options);
		}
	});
});

_commander2.default.on('--help', function () {
	console.log('  Examples:');
	console.log('');
	console.log('    $ pdt install mypackage (clones the package to your packages directory)');
	console.log('    $ pdt create mypackage -l (clones the package to your packages directory, and links any depdencies found in the pakcage directory)');
	console.log('');
});

_commander2.default.parse(process.argv);