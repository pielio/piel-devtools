#!/usr/bin/env node
'use strict';

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _co = require('co');

var _co2 = _interopRequireDefault(_co);

var _PielDT = require('./libs/PielDT');

var _PielDT2 = _interopRequireDefault(_PielDT);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_commander2.default.arguments('<url>').option('--replace', 'Replace existing components').option('-p, --project', 'Clone as project (clones to your project directory, not your development directory)').option('-i, --install', 'Install node_modules (yarn install) after cloning is complete').option('-l, --link', 'Link all development components after install').action((url, commander) => {

	(0, _co2.default)(function* () {

		let options = {
			url: url,
			replace: !!_commander2.default.replace,
			project: !!_commander2.default.project,
			install: !!_commander2.default.install,
			link: !!_commander2.default.link
		};

		if (!options.install && options.link) {
			console.log(chalk.red.bold('Aborting.. argue --install if you want the clone to be linked'));
			process.exit();
		}

		const Instance = new _PielDT2.default();
		if (url === 'all') {
			Instance.cloneAll(options);
		} else {
			Instance.clone(options);
		}
	}).catch(err => {
		console.log('Exception error', err);
	});
});

_commander2.default.on('--help', function () {
	console.log('  Examples:');
	console.log('');
	console.log('    $ pdt clone mymodule (performs a git clone to the development directory)');
	console.log('    $ pdt clone --project myproject (performs a git clone to the project directory)');
	console.log('    $ pdt clone --project -i myproject (performs a git clone to the project directory, then installs it.)');
	console.log('    $ pdt clone --project -i myproject (performs a git clone to the project directory, then installs it , and finally links all developmental components to it)');
	console.log('');
});

_commander2.default.on('--help', function () {
	console.log('  Examples:');
	console.log('');
	console.log('    $ pdt clone git@bitbucket.org/myteam/package.git [clone a package to the packages directory]');
	console.log('    $ pdt clone git@bitbucket.org/myteam/package.git -i [clone a package to the packages directory, and run yarn install over it]');
	console.log('    $ pdt clone git@bitbucket.org/myteam/package.git -il [clone a package to the packages directory, run yarn install over it, then yarn link everything found in the packages directory to it.');
	console.log('    $ pdt clone git@bitbucket.org/myteam/project.git -p [clone a project to the project directory]');

	console.log('');
});

_commander2.default.parse(process.argv);