#!/usr/bin/env node
'use strict';

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _co = require('co');

var _co2 = _interopRequireDefault(_co);

var _PielDT = require('./libs/PielDT');

var _PielDT2 = _interopRequireDefault(_PielDT);

var _chalk = require('chalk');

var _chalk2 = _interopRequireDefault(_chalk);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_commander2.default.arguments('<package name>').action((name, commander) => {

	(0, _co2.default)(function* () {

		const Instance = new _PielDT2.default();

		if (name.toUpperCase() === 'ALL') {
			yield Instance.allLinksInfo();
		} else {
			yield Instance.linkInfo({ name: name });
		}

		process.exit();
	}).catch(err => {
		console.log('EDT linkinfo failed with: ', err);
	});
});

_commander2.default.on('--help', function () {
	console.log('  Examples:');
	console.log('');
	console.log('    $ pdt linkinfo mypackage');
	console.log('');
});

_commander2.default.parse(process.argv);