#!/usr/bin/env node
'use strict';

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _co = require('co');

var _co2 = _interopRequireDefault(_co);

var _chalk = require('chalk');

var _chalk2 = _interopRequireDefault(_chalk);

var _PielDT = require('./libs/PielDT');

var _PielDT2 = _interopRequireDefault(_PielDT);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_commander2.default.arguments('<dependency> <packagename> [version]').option('-f, --force', 'assume yes for all prompts.').option('-c, --component', 'update components to the new dependency version').option('-p, --project', 'update projects to the new dependency version').option('-t, --template', 'update templates to the new dependency version').option('-a, --all', 'apply the update to both components and projects, effectively updating versions across the board').action((dependency, packagename, version, commander) => {

	version = version || 'latest';

	const {
		force,
		component,
		project,
		template,
		all } = _commander2.default;

	let args = {
		name: dependency,
		pkg: packagename,
		version: version,
		force: !!force,
		component: !!component,
		project: !!project,
		templates: !!template,
		all: !!all
	};

	(0, _co2.default)(function* () {

		const Instance = new _PielDT2.default();

		let update_type;

		if (dependency.toUpperCase() === 'ALL' && packagename.toUpperCase() === 'ALL') update_type = 'many_many';else if (dependency.toUpperCase() === 'ALL') update_type = 'many_one';else if (packagename.toUpperCase() === 'ALL') update_Type = 'one_many';else update_type = 'one_one';

		switch (update_type) {

			case 'many_many':
				console.log('Updating all packages and all dependencies is unsupported atm - soon!');
				break;

			case 'many_one':
				yield Instance.updateAllDependencies(args);
				break;

			case 'one_many':
				yield Instance.updateDependencies(args);
				break;

			case 'one_one':
				yield Instance.updateDependency(args);
				break;

		}
		process.exit(1);
	}).catch(err => {
		console.log('pdt update failed with an exception: ', err);
	});

	return;
});

_commander2.default.on('--help', function () {
	console.log('  Examples:');
	console.log('');
	console.log('    $ pdt update react mypackage (updates react to the latest version in package mypackage)');
	console.log('    $ pdt update react all (updates react to the latest version in all packages found in the package directory)');
	console.log('    $ pdt update all mypackage (updates all dependencies to the latest version for the mypackage in the package directory)');
	console.log('    $ pdt update all myproject -p (updates all dependencies to the latest version for "myproject" in the project directory)');
	console.log('    $ pdt update all all (updates all dependencies for all packages in the package directory)');
	console.log('');
});

_commander2.default.parse(process.argv);