#!/usr/bin/env node
'use strict';

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _co = require('co');

var _co2 = _interopRequireDefault(_co);

var _PielDT = require('./libs/PielDT');

var _PielDT2 = _interopRequireDefault(_PielDT);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_commander2.default.arguments('<name>').option('-r, --recursive', 'get information about depedencies').action((name, commander) => {

	const options = {
		name: name,
		recursive: !!_commander2.default.recursive
	};

	(0, _co2.default)(function* () {

		const PDT = new _PielDT2.default();
		yield PDT.packageInformation(options);
		process.exit();
	});
});

_commander2.default.parse(process.argv);

_commander2.default.on('--help', function () {
	console.log('  Examples:');
	console.log('');
	console.log('    $ pdt info mypackage');
	console.log('    $ pdt link mypackage -r (provides information all the way down the dep tree for modules found in your verdaccio server)');
	console.log('');
});