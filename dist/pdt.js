#!/usr/bin/env node
'use strict';

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_commander2.default.version('0.0.1').command('setup', 'configure PDT for your environment').command('config', 'configure').command('info', 'get package information').command('create', 'create a package from a template').command('install', 'install components').command('publish', 'publish components').command('watch', 'listen for changes on a component').command('clone', 'clone components to your component directory').command('update', 'update a components dependecy to a version').command('link', 'link packages').command('linkinfo', 'get information about yarn linked packages').parse(process.argv);