#!/usr/bin/env node
'use strict';

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _co = require('co');

var _co2 = _interopRequireDefault(_co);

var _PielDT = require('./libs/PielDT');

var _PielDT2 = _interopRequireDefault(_PielDT);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_commander2.default.arguments('<action> [variable] [value]').action((action, variable, value, commander) => {

	return (0, _co2.default)(function* () {

		const Instance = new _PielDT2.default();

		switch (action) {

			case 'list':
				yield Instance.listConfig();
				process.exit(1);

			case 'set':
				yield Instance.setConfigVariable({ variable: variable, value: value });
				process.exit(1);

			case 'unset':
				yield Instance.unsetConfigVariable({ variable: variable });
				process.exit(1);

			case 'add':
				switch (variable) {

					case 'template':
						yield Instance.setupTemplate({ name: value });
						process.exit(1);

					default:
						console.log('PDT config add - unknown type ' + variable);
						process.exit(1);
				}

			case 'remove':
				switch (variable) {

					case 'template':
						yield Instance.deleteTemplate({ name: value });
						process.exit(1);

					default:
						console.log('PDT config add - unknown type ' + variable);
						process.exit(1);
				}

			case 'feature':
				switch (variable) {
					case 'slack':
						yield Instance.setup_slack(null, true);
						process.exit(1);
				}
			case 'features':
				console.log('PDT supported features: slack');

			default:
				console.log('PDT config -- unknown action ' + action);
				process.exit(1);
		}
	}).catch(err => {
		console.log('pdt-config exception error:', err);
	});
});

_commander2.default.on('--help', function () {
	console.log('  Examples:');
	console.log('');
	console.log('    $ pdt config list (List the pdf config)');
	console.log('    $ pdt config set <option> <value> (set an value for an option)');
	console.log('    $ pdt config unset <option> (removes an option from the configuration)');
	console.log('    $ pdt config features (list available feature modules)');
	console.log('    $ pdt config feature <feature> (run the configuration for an argued feature module)');
	console.log('    $ pdt config add template <mygitrepo> (add a known starter kit --- experimental!)');
	console.log('    $ pdt config remove template <mygitrepo> (remove a known starter kit --- experimental!)');
	console.log('');
});
_commander2.default.parse(process.argv);