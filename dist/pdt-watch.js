#!/usr/bin/env node
'use strict';

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _co = require('co');

var _co2 = _interopRequireDefault(_co);

var _PielDT = require('./libs/PielDT');

var _PielDT2 = _interopRequireDefault(_PielDT);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/** @TODO tests! **/
_commander2.default.arguments('[action]').action((action, commander) => {

	(0, _co2.default)(function* () {

		const Instance = new _PielDT2.default();

		switch (action) {

			case 'compile':
				Instance.watch({ action: ['compile'] });

		}
	}).catch(err => {
		console.log('oppz somethign went wrong', err);
	});
});

_commander2.default.on('--help', function () {
	console.log('  Examples:');
	console.log('');
	console.log('    $ pdt watch compile (compiles packages whenever their /src directory is modified) ');
	console.log('');
});

_commander2.default.parse(process.argv);