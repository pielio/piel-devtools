'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _co = require('co');

var _co2 = _interopRequireDefault(_co);

var _coPrompt = require('co-prompt');

var _coPrompt2 = _interopRequireDefault(_coPrompt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class GithubAPI {

	constructor() {}

	initialize(config) {
		this._config = config;
	}

	setup() {

		return (0, _co2.default)(function* () {

			console.log('Github not implemented yet. Dont worry! You will be prompted to complete steps until it is.');
			return this._config;
		}.bind(this));
	}

	/**
 * Create a repository on Github
 *
 * @param {object} args the argument object.
 * @param {string} args.name the name for the new repository
 *
 **/
	create_repo(args) {

		return (0, _co2.default)(function* () {

			console.log('Automated creation of Github repositories is not enabled yet. Manually create the repository `' + name + '` through the interface before continuing.');
			yield (0, _coPrompt2.default)('Done? [YES/no]:');
		}.bind(this));
	}
}
exports.default = GithubAPI;