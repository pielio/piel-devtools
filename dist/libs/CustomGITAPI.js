'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _co = require('co');

var _co2 = _interopRequireDefault(_co);

var _coPrompt = require('co-prompt');

var _coPrompt2 = _interopRequireDefault(_coPrompt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class CustomGITAPI {

	constructor() {}

	initialize(config) {
		this._config = config;
	}

	setup() {

		return (0, _co2.default)(function* () {

			console.log('Custom Git API not implemented yet. Dont worry! You will be prompted to complete steps until it is.');
			return this._config;
		}.bind(this));
	}

	/**
 * Create a repository on Custom GIT servers.
 *
 * @param {object} args the argument object.
 * @param {string} args.name the name for the new repository
 *
 **/
	create_repo(args) {

		return (0, _co2.default)(function* () {

			console.log('Automated creation of repositories is not enabled yet. Manually create the repository `' + name + '` on your git server.');
			yield (0, _coPrompt2.default)('Done? [YES/no]:');
		}.bind(this));
	}
}
exports.default = CustomGITAPI;