'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _co = require('co');

var _co2 = _interopRequireDefault(_co);

var _coPrompt = require('co-prompt');

var _coPrompt2 = _interopRequireDefault(_coPrompt);

var _chalk = require('chalk');

var _chalk2 = _interopRequireDefault(_chalk);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class BitbucketAPI {

	constructor() {}

	initialize(config) {

		this._config = config;
	}

	setup() {

		return (0, _co2.default)(function* () {

			console.log('Bitbucket not implemented yet.  Dont worry! You will be prompted to complete steps until it is.');
			return this._config;
		}.bind(this));
	}

	/**
 * Create a repository on Bitbucket
 *
 * @param {object} args the argument object.
 * @param {string} args.name the name for the new repository
 *
 **/
	create_repo(args) {

		return (0, _co2.default)(function* () {

			const { name } = args;
			console.log('Automated creation of Bitbucket repositories is not enabled yet. Manually create the repository `' + name + '` through the interface before continuing.');
			yield (0, _coPrompt2.default)('Repository ready? [YES/no]:');

			let done = false;
			let url, confirm;

			while (!done) {

				url = yield (0, _coPrompt2.default)('Paste the provided URL here: ');
				console.log(_chalk2.default.yellow('Using ' + url));

				confirm = yield (0, _coPrompt2.default)('Correct? [YES/no]:');
				if (!confirm || confirm.toUpperCase() === 'YES') {
					done = true;
				} else {
					console.log('lets try again..');
				}
			}

			return url;
		}.bind(this));
	}
}
exports.default = BitbucketAPI;