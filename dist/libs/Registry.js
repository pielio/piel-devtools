'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _co = require('co');

var _co2 = _interopRequireDefault(_co);

var _child_process = require('child_process');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class Registry {

	constructor(config) {

		this._config = config;
	}

	/**
 * Get the current configured registry.
 *
 * @return {string} the current registry, from the instance config.
 **/
	configured_registry() {

		return (0, _co2.default)(function* () {

			const url = this._config.registry_url;
			return url.substr(url.length - 1) === '/' ? url.substr(0, url.length - 1) : url;
		}.bind(this));
	}

	/**
 * Get the current registry in use by the runtime.
 *
 * @return {string} the curren tregistry, obtained from yarn.
 **/
	current_registry() {

		return (0, _co2.default)(function* () {

			let response = (0, _child_process.execSync)('yarn config get registry');
			let url = response.toString().replace(/(?:\r\n|\r|\n)/g, '');
			return url.substr(url.length - 1) === '/' ? url.substr(0, url.length - 1) : url;
		}.bind(this));
	}

	/**
 * Check to see the runtime is using the configured registry.
 *
 * @return {boolean} true/false.
 **/
	using_configured_registry() {

		return (0, _co2.default)(function* () {

			const current = yield this.current_registry();
			const configured = yield this.configured_registry();

			return current === configured;
		}.bind(this));
	}

}
exports.default = Registry;