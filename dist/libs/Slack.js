'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _co = require('co');

var _co2 = _interopRequireDefault(_co);

var _requestPromiseNative = require('request-promise-native');

var _requestPromiseNative2 = _interopRequireDefault(_requestPromiseNative);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class Slack {

	constructor(config) {

		this._config = config;
		console.log('CONSTRTUCTED SLACK', JSON.stringify(this._config));
	}

	/**
  * 
  * @param {Object} args the argument object.
  * @param {String} args.text the message. 
  */
	message(args) {

		//https://api.slack.com/incoming-webhooks

		return (0, _co2.default)(function* () {

			const { text, emoji } = args;
			const { slack_hook } = this._config;

			let body = {};
			if (text) body.text = text;

			const response = yield (0, _requestPromiseNative2.default)({
				method: 'POST',
				url: slack_hook,
				body: JSON.stringify(body)
			});

			return response;
		}.bind(this));
	}

}
exports.default = Slack;