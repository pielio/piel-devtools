#!/usr/bin/env node
'use strict';

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _co = require('co');

var _co2 = _interopRequireDefault(_co);

var _PielDT = require('./libs/PielDT');

var _PielDT2 = _interopRequireDefault(_PielDT);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_commander2.default.arguments('<name> <template>').option('-p, --project', 'Create the cloned template in the project directory').option('-l, --link', 'after creation, link everything found in the packages directory to it').option('-d, --develop', 'set the cloned template to a development mode - ie. yarn link <name>').action((name, template, commander) => {

	(0, _co2.default)(function* () {

		const { project, link, develop } = _commander2.default;

		let args = {
			name: name,
			template: template,
			project: !!project,
			link: !!link,
			develop: !!develop
		};

		const Instance = new _PielDT2.default();

		Instance.cloneFromTemplate(args);
	}).catch(err => {
		console.log('PDT::create failed with an exception error', err);
	});
});

_commander2.default.on('--help', function () {
	console.log('  Examples:');
	console.log('');
	console.log('    $ pdt create mypackage react-component  (clones the "react-component" template to the packages directory as "mypackage", updating the package.json)');
	console.log('    $ pdt create myproject react-project -p (clones the "react-project" template to the project directory as "myproject", updating the package.json)');
	console.log('    $ pdt create myproject react-project -pl (clones the "react-project" template to the project directory as "myproject", updating the package.json, AND linking every package found in the package directory)');
	console.log('');
});

_commander2.default.parse(process.argv);