#!/usr/bin/env node
'use strict';

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _co = require('co');

var _co2 = _interopRequireDefault(_co);

var _PielDT = require('./libs/PielDT');

var _PielDT2 = _interopRequireDefault(_PielDT);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _co2.default)(function* () {

	const PDT = new _PielDT2.default();
	yield PDT.setup();
	process.exit();
}).catch(err => {
	console.log('Setup Failed:::', err);
});