import { assert } from 'chai';
import Slack from '../src/libs/Slack';

import co from 'co';

import config from './mocks/slack_config';

describe( 'Tests the library class', () => {

	let Instance;

	describe( 'Packages class instantiation and configuration', () => {

		it( 'Instantiates the class' , () => {

			Instance = new Slack(config);
			assert( Instance instanceof Slack, 'Class could not be instantiated' );
		});

		it( 'Configured the class upon instantiation', () => {

			assert( Instance._config, 'Class did not receive a config');
			assert( Instance._config.slack_enabled !== undefined, 'Class did not receive a slack_enabled flag in the config');
			assert( Instance._config.slack_webhook, 'Class did not receive a dev_directory in the config');
		});

	});

	describe('Methods', () => {

		describe('Slack::message', () => {

			it('Sends a message through the webhook', () => {

				return Instance.message({ text: 'Chai Test\nMultiline\n*test*' })
					.then( resp => {
						assert(resp === 'ok', 'Did not respond with a string of "ok"');
					})

			});

		});

	});

});
